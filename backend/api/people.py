from db import db

from flask import request
from flask_restful import Resource


def person_info_is_valid(info):
    keys = info.keys()
    return len(keys) == 2 and "name" in keys and "dob" in keys


class People(Resource):
    def get(self):
        rows = db.execute_select("select person_id, person_name, person_dob, count(faces_in_pictures.fip_id) from people left join faces_in_pictures on person_id=fip_person_id group by person_id")

        return [
            {
                "id": r[0],
                "name": r[1],
                "dob": r[2],
                "count": r[3]
            }
            for r in rows
        ]

    def post(self):
        json_data = request.get_json(force=True)
        if not person_info_is_valid(json_data):
            return ("Bad person info format", 400)

        id = db.insert_values("people", {
            "person_name": json_data["name"],
            "person_dob": json_data["dob"]
        })

        return {"id": id}


class Person(Resource):
    def get(self, person_id):
        rows = db.execute_select("select person_id, person_name, person_dob from people where person_id=:person_id", person_id=person_id)

        if len(rows) == 0:
            return ("No person with this id", 404)

        r = rows[0]
        return {
            "id": r[0],
            "name": r[1],
            "dob": r[2]
        }

    def put(self, person_id):
        json_data = request.get_json(force=True)
        if not person_info_is_valid(json_data):
            return ("Bad person info format", 400)

        db.execute_and_commit("update people set person_name=:person_name, person_dob=:person_dob",
                              person_name=json_data["name"],
                              person_dob=json_data["dob"])

        return self.get(person_id)

    def delete(self, person_id):
        # TODO: should be in a single transaction
        c = db.connect()
        c.execute("delete from people where person_id=:person_id", person_id=person_id)
        c.execute("update faces_in_pictures set person_id=NULL where person_id=:person_id", person_id=person_id)
        c.commit()
        return "deleted"
