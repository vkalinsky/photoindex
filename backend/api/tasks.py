from flask_restful import Resource
from redis_util import Task

class Tasks(Resource):
    def get(self):
        return [
            {
                "id": id,
                **t.to_dict()
            } for id, t in Task.get_all_tasks().items()
        ]

