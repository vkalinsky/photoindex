import os
from flask import send_file
from flask_restful import Resource
from db import storage


class Thumbnails(Resource):
    def get(self, item_id):
        thumbnailPath = storage.thumbnailFilename(item_id)
        if not os.path.isfile(thumbnailPath):
            return ("No thumbnail for this item", 404)

        return send_file(thumbnailPath, mimetype='image/jpeg', as_attachment=False)

    def delete(self, item_id):
        thumbnailPath = storage.thumbnailFilename(item_id)
        if os.path.isfile(thumbnailPath):
            os.remove(thumbnailPath)
            return "Removed"
        else:
            return ("Thumbnail does not exist", 404)

class Filmstrips(Resource):
    def get(self, item_id):
        filmstripPath = storage.filmstripFilename(item_id)
        if not os.path.isfile(filmstripPath):
            return ("No filmstrip for this item", 404)

        return send_file(filmstripPath, mimetype='image/jpeg', as_attachment=False)

    def delete(self, item_id):
        filmstripPath = storage.filmstripFilename(item_id)
        if os.path.isfile(filmstripPath):
            os.remove(filmstripPath)
            return "Removed"
        else:
            return ("Filmstrip does not exist", 404)