import os
from dataclasses import dataclass
from flask import request
from flask_restful import Resource
from db import db, storage
from redis_util import Task

from . import albums

def parse_filter(filter: str) -> str:
    """
        "with faces"
        "without faces"
        "received media"
    """
    where = "true"

    if filter == "only photos":
        where = "fs_media_type = 'image'"
    elif filter == "only videos":
        where = "fs_media_type = 'video'"
    elif filter == "geotagged":
        where = "(fs_gps_lat is not null or fs_id in (select gt_fs_id from geo_tags_v1))"
    elif filter == "not geotagged":
        where = "fs_gps_lat is null and fs_id not in (select gt_fs_id from geo_tags_v1)"
    elif filter == "with faces":
        where = "fs_media_type = 'image' and fs_id not in (select distinct(fip_fs_id) from faces_in_pictures where fip_encodings is null)"
    elif filter == "without faces":
        where = where = "fs_media_type = 'image' and fs_id in (select distinct(fip_fs_id) from faces_in_pictures where fip_encodings is null)"
    elif filter == "received media":
        where = "fs_date_taken is null"
    elif filter == "organized by dates":
        where = "fs_date_taken is not null"

    return where

class Directories(Resource):
    def __init__(self):
        self._dirs = {}

    def _create_dir_entry(self, root, path, entry_info):
        comps = path.split('/')

        dirs = root.get("dirs", [])
        root["dirs"] = dirs

        ei_date_of_last_item = entry_info["date_of_last_item"] or ""

        dir = comps[0]
        entry = next((d for d in dirs if d["name"] == dir), None)
        if entry is not None:
            entry["date_of_last_item"] = max(entry["date_of_last_item"] or "", ei_date_of_last_item)
        else:
            entry = {}
            entry["name"] = dir
            entry["date_of_last_item"] = ei_date_of_last_item
            dirs.append(entry)

        if len(comps) > 1:
            self._create_dir_entry(entry, '/'.join(comps[1:]), entry_info)
        else:
            entry["number_of_files"] = entry_info["number_of_files"]

    def get_by_dates(self):
        dirs = {}
        for d, c, max_date in db.execute_select('select strftime("/%Y/%m", fs_date_taken) as d, count(*) as c, max(fs_date_taken) from files group by d order by fs_date_taken desc'):
            if d is None:
                continue

            dir_info = {
                "path": d,
                "number_of_files": c,
                "date_of_last_item": max_date,
            }
            self._create_dir_entry(dirs, d, dir_info)

        return dirs["dirs"]

    def get(self):
        # Get filter parameter from query string
        where = parse_filter(request.args.get("filter", ""))
        if where != "":
            where = "where " + where

        query = f"select fs_dir, count(*), max(fs_date_taken) as dt from files {where} group by fs_dir order by dt desc"

        dirs = {}
        for row in db.execute_select(query):
            d = {
                "path": row[0],
                "number_of_files": row[1],
                "date_of_last_item": row[2],
            }
            self._create_dir_entry(dirs, row[0], d)

        return dirs["dirs"]

@dataclass
class TimeRange:
    start: int
    end: int

@dataclass
class DirInfo:
    title: str
    is_album: bool
    album_id: str
    number_of_files: int
    time_range: TimeRange

    def __dict__(self):
        return {
            "title": self.title,
            "is_album": self.is_album,
            "album_id": self.album_id,
            "number_of_files": self.number_of_files,
            "time_range": {
                "start": self.time_range.start,
                "end": self.time_range.end,
            }
        }

class ItemsInDir(Resource):
    def __init__(self):
        self._cols = [
            "fs_id",
            "fs_filename",
            "fs_dir",
            "fs_file_size",
            "fs_date_taken",
            "coalesce(fs_gps_lat, gt_lat) as fs_gps_lat",
            "coalesce(fs_gps_lon, gt_lon) as fs_gps_lon",
            "fs_frame_width",
            "fs_frame_height",
            "fs_media_type",
            "fs_orientation",
            "star_fs_id",
            "gt_lon",
            "ia_album_id",
        ]

    def get(self, path):
        path = path.replace(":", "/")

        query_parts = []
        query_parts.append("select %s from files" % ','.join(self._cols))
        query_parts.append("left join deleted_file_ids on fs_id=del_fs_id")
        query_parts.append("left join starred_file_ids on fs_id=star_fs_id")
        query_parts.append("left join geo_tags_v1 on fs_id=gt_fs_id")
        query_parts.append("left join items_in_albums_v1 on fs_id=ia_fs_id")

        where_parts = []

        dirInfo = DirInfo("", False, "", 0, TimeRange(0,0))

        filter = request.args.get("filter", "")
        if filter != "":
            where_parts.append(parse_filter(filter))

        max_items = 2000
        order_by = "coalesce(fs_date_taken, fs_date_import) desc"

        if path == '.trash':
            where_parts.append("del_fs_id is not NULL")
            order_by = "del_fs_timestamp desc"
            dirInfo.title = "Deleted items"
        elif path == '.random':
            where_parts.append("del_fs_id is NULL")
            order_by = "random()"
            max_items = 100
            dirInfo.title = "Random items"
        else:
            if path == '.starred':
                where_parts.append("star_fs_id is not NULL")
                order_by = "star_fs_timestamp desc"
                dirInfo.title = "Starred items"
            elif path == '.recentlyadded':
                order_by = "fs_date_import desc"
                max_items = 200
                dirInfo.title = f"{max_items} last added items"
            elif path.startswith('.album/'):
                album_id = path.split('/')[1]
                album_filters = ["ia_album_id = %s" % album_id]
                album = albums.get_by_id(album_id)
                dirInfo.title = album.name
                dirInfo.is_album = True
                dirInfo.album_id = album_id

                if album is not None and album.query is not None:
                    album_filters.append("(" + album.query + ")")

                where_parts.append("(" + " or ".join(album_filters) + ")")
            else:
                dirInfo.title = f"{path} folder"
                where_parts.append("fs_dir like :path")

            where_parts.append("del_fs_id is NULL")

        query_parts.append("where " + ' and '.join(where_parts))
        query_parts.append(f"order by {order_by}")
        query_parts.append("limit %d" % max_items)

        query = ' '.join(query_parts)

        # Build list of active tasks to augment the results
        tasks = {}
        for _, task in Task.get_all_tasks().items():
            if task.item_id not in tasks:
                tasks[task.item_id] = []

            tasks[task.item_id].append(task.to_dict())

        images = []
        all_album_ids = [a.id for a in albums.get_all_albums()]
    
        colnames = [c.split(' ')[-1] for c in self._cols]

        # Left join albums duplicates items, so we'll be storing the items we've already seen.
        items_already_added = {}
        for row in db.execute_select(query, path=path):
            im = {}

            row_obj = dict(zip(colnames, list(row)))

            for k, v in row_obj.items():
                if v is not None:
                    if k == "star_fs_id":
                        im["starred"] = True
                    elif k == "ia_album_id":
                        continue  # We'll handle this later
                    else:
                        im[k.replace("fs_", "")] = v

            if im["id"] in items_already_added:
                im = items_already_added[im["id"]]
            else:
                im["optimized"] = os.path.isfile(storage.optimizedFilename(im["id"], im["media_type"]))

                if im["media_type"] == "video":
                    im["has_filmstrip"] = os.path.isfile(storage.filmstripFilename(im["id"]))

                if im["id"] in tasks:
                    im["tasks"] = tasks[im["id"]]

                if row_obj["gt_lon"] != None:
                    im["geo_tagged"] = True
                    del im["gt_lon"]

                images.append(im)
                items_already_added[im["id"]] = im

            album_id = str(row_obj["ia_album_id"])
            if album_id is not None and album_id in all_album_ids:
                if "albums" not in im:
                    im["albums"] = []

                if album_id not in im["albums"]:
                    im["albums"].append(album_id)

        min_time = None
        max_time = None
        for im in images:
            if "date_taken" not in im:
                continue

            if im["date_taken"] != None:
                if min_time == None or im["date_taken"] < min_time:
                    min_time = im["date_taken"]

                if max_time == None or im["date_taken"] > max_time:
                    max_time = im["date_taken"]

        dirInfo.number_of_files = len(images)
        dirInfo.time_range = TimeRange(min_time, max_time)

        return {
            "dir_info": dirInfo.__dict__(),
            "items": images,
        }
