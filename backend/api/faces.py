import os
import json
from flask import send_file, request
from flask_restful import Resource
from PIL import Image, ImageDraw
import numpy as np
from db import db, storage, utility
import config
from . import utils


class ImageFacesSingle(Resource):
    def __init__(self):
        self._cols = [
            "fip_id",
            "fip_top",
            "fip_left",
            "fip_right",
            "fip_bottom",
            "fip_encodings",
            "fip_landmarks",
            "fip_person_id",
            "fip_person_id_guess",
            "fip_ignored",
            "fip_fs_id",
        ]
        self._str_cols = ','.join(self._cols)

    def get(self, fip_id):
        rows = db.execute_select("select %s from faces_in_pictures where fip_id=:fip_id" % self._str_cols, fip_id=fip_id)

        if len(rows) == 0:
            return ("Item not found", 404)

        if rows[0][1] is None:
            return {}

        face = {}
        row_dict = dict(zip(self._cols, list(rows[0])))
        for k, v in row_dict.items():
            if k in ["fip_landmarks", "fip_encodings"]:
                v = json.loads(v)

            if k == "fip_ignored":
                v = v == 1

            face[k.replace("fip_", "")] = v

        return face

    def tag_face(self, ids, person_id):
        query_ids = ','.join(map(str, ids))
        query = "update faces_in_pictures set fip_person_id=:person_id, fip_ignored=0 where fip_id in (%s)" % query_ids
        db.execute_and_commit(query, person_id=person_id)
        return "ok"

    def set_ignored_flag(self, ids, ignored):
        query_ids = ','.join(map(str, ids))
        query = "update faces_in_pictures set fip_ignored=:ignored, fip_person_id=null where fip_id in (%s)" % query_ids
        db.execute_and_commit(query, ignored=ignored)
        return "ignored: " + str(ignored)

    def put(self, fip_id="batch"):
        json_data = request.get_json(force=True)

        if fip_id == "batch":
            if "ids" not in json_data:
                return ("No ids in batch request)", 400)

            ids = json_data["ids"]
        else:
            ids = [fip_id]

        for id in ids:
            c = db.execute_select("select fip_id from faces_in_pictures where fip_id=:fip_id limit 1", fip_id=id)
            if len(c) == 0:
                return ("Item with id %s not found" % id, 404)

        if "person_id" in json_data:
            person_id = json_data["person_id"]
            c = db.execute_select("select person_id from people where person_id=:person_id limit 1", person_id=person_id)
            if len(c) == 0:
                return ("Person with id %d not found" % person_id, 404)
            return self.tag_face(ids, person_id)
        elif "ignore" in json_data:
            return self.set_ignored_flag(ids, json_data["ignore"])

        return ('Wrong payload format, expected { "person_id": id } or { "ignore": true/false }', 400)


class ImageFacesThumbnails(Resource):
    def get(self, fip_id):
        cols = [
            "fip_id",
            "fip_fs_id",
            "fip_top",
            "fip_left",
            "fip_right",
            "fip_bottom",
        ]
        str_cols = ','.join(cols)
        rows = db.execute_select("select %s from faces_in_pictures where fip_id=:fip_id" % str_cols, fip_id=fip_id)
        if len(rows) == 0:
            return ("Face in picture with this id is not found", 404)

        _, item_id, fip_top, fip_left, fip_right, fip_bottom = rows[0]

        if fip_top is None:
            return ("Face with this id is empty", 404)

        previewfile = storage.facepreviewFilename(fip_id)

        if os.path.exists(previewfile):
            return send_file(previewfile, mimetype='image/jpeg', as_attachment=False)

        img_filename = storage.getOriginalFilename(item_id)
        with open(img_filename, "r") as f:
            img = Image.open(f)
            w, h = img.size
            fw = (fip_right - fip_left) * w
            fh = (fip_bottom - fip_top) * h
            expand_left = fw * config.face_preview_expand_ratio["left"] / 2
            expand_right = fw * config.face_preview_expand_ratio["right"] / 2
            expand_top = fh * config.face_preview_expand_ratio["top"] / 2
            expand_bottom = fh * config.face_preview_expand_ratio["bottom"] / 2

            crop_box = (
                max(0, fip_left * w - expand_left),
                max(0, fip_top * h - expand_top),
                min(w, fip_right * w + expand_right),
                min(h, fip_bottom * h + expand_bottom)
            )

            img = img.crop(box=crop_box)

            if max(img.size) > config.face_preview_max_size:
                img.thumbnail((config.face_preview_max_size, config.face_preview_max_size))

            previewfileDir = os.path.dirname(previewfile)
            if not os.path.isdir(previewfileDir):
                os.makedirs(previewfileDir)

            with open(previewfile, "w") as f:
                img.save(f)

            return utils.serve_pil_image(img)


class GetBatchOfUntagged(Resource):
    def get(self):
        batch_size = 10

        query = ' '.join([
            "select fip_id from faces_in_pictures",
            "where fip_ignored != 1",
            " and fip_person_id is null",
            " and fip_person_id_guess in",
            "(",
                "select fip_person_id from faces_in_pictures",
                "where fip_person_id is not null",
                "group by fip_person_id",
                "having count(fip_person_id)<20",
            ")",
            "order by fip_fs_id",
            "limit :batch_size"
        ])

        return [fip_id for (fip_id,) in db.execute_select(query, batch_size=batch_size)]


class GetFaceContext(Resource):
    def get(self, fip_id):
        query = "select fip_fs_id, fip_top, fip_bottom, fip_left, fip_right from faces_in_pictures where fip_id=:fip_id"

        rows = db.execute_select(query, fip_id=fip_id)
        if len(rows) == 0:
            return ("No face with this id", 404)

        item_id, top, bottom, left, right = rows[0]

        media_type, orientation = db.fetchItemInfo(item_id, ["fs_media_type", "fs_orientation"])
        if media_type != "image":
            return ("Media type %s is not supported" % media_type, 400)

        filename = storage.getOriginalFilename(item_id)
        if not os.path.exists(filename):
            return ("File for id %s not found" % item_id, 500)

        img = Image.open(filename)
        img_w, img_h = img.size

        w, h = utility.transformFrameSize(img_w, img_h, orientation)
        w = float(w)
        h = float(h)

        max_dim = max(w, h)
        scale_factor = 640.0 / max_dim
        if scale_factor < 1:
            img = img.resize((int(w * scale_factor), int(h * scale_factor)), Image.ANTIALIAS)
            img_w, img_h = img.size

        draw = ImageDraw.Draw(img)
        rect_xy = [
            left * img_w,
            top * img_h,
            right * img_w,
            bottom * img_h
        ]

        draw.rectangle(rect_xy, outline="white", width=5)

        return utils.serve_pil_image(img)


def floatsFromJson(str):
    str_values = json.loads(str)
    return np.asarray([float(v) for v in str_values])


class FindSimilar(Resource):
    def get(self, fip_id):
        rows = db.execute_select("select fip_encodings from faces_in_pictures where fip_id=:fip_id and fip_encodings is not null", fip_id=fip_id)

        if len(rows) == 0:
            return ("No face with this id", 404)

        ref_encodings = floatsFromJson(rows[0][0])

        untaggedOnly = int(request.args.get("untagged", 0)) == 1
        unguessedOnly = int(request.args.get("unguessed", 0)) == 1

        query_comp = [
            "select",
            "fip_id, fip_encodings, fip_ignored, fip_person_id, fip_person_id_guess",
            "from faces_in_pictures",
            "where",
            "fip_id!=:fip_id",
            "and fip_encodings is not null",
            "and fip_person_id is null" if untaggedOnly else "",
            "and fip_person_id_guess is null" if unguessedOnly else ""
        ]

        query = ' '.join(query_comp)
        rows = db.execute_select(query, fip_id=fip_id)

        matched_encodings = []
        for r in rows:
            id, str_encodings, ignored, person_id, person_id_guess = r
            encodings = floatsFromJson(str_encodings)
            d = np.linalg.norm(ref_encodings - encodings)
            if d < 0.5:
                matched_encodings.append({
                    "id": id,
                    "ignored": ignored,
                    "person_id": person_id,
                    "person_id_guess": person_id_guess,
                    "distance": d
                })

        matched_encodings = sorted(matched_encodings, key=lambda enc: enc["distance"])[:100]

        return matched_encodings
