from timeit import default_timer as timer
from flask import send_file
from PIL import Image
from io import BytesIO


class benchmark(object):
    def __init__(self, msg, fmt="%0.3g"):
        self.msg = msg
        self.fmt = fmt

    def __enter__(self):
        self.start = timer()
        return self

    def __exit__(self, *args):
        t = timer() - self.start
        print(("%s : " + self.fmt + " seconds") % (self.msg, t))
        self.time = t


def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, 'JPEG', quality=85)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')


def rotate_pil_image(im, orientation):
    """
    From https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
        1 = Horizontal (normal)
        2 = Mirror horizontal
        3 = Rotate 180
        4 = Mirror vertical
        5 = Mirror horizontal and rotate 270 CW (90 CCW)
        6 = Rotate 90 CW (270 CCW)
        7 = Mirror horizontal and rotate 90 CW (270 CCW)
        8 = Rotate 270 CW (90 CCW)
    """

    transpose = {
        1: [],
        2: [Image.FLIP_LEFT_RIGHT],
        3: [Image.ROTATE_180],
        4: [Image.FLIP_TOP_BOTTOM],
        5: [Image.FLIP_LEFT_RIGHT, Image.ROTATE_90],
        6: [Image.ROTATE_270],
        7: [Image.FLIP_TOP_BOTTOM, Image.ROTATE_90],
        8: [Image.ROTATE_90],
    }

    tr = transpose[orientation]
    for t in tr:
        im = im.transpose(t)

    return im
