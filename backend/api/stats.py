import os
from flask_restful import Resource
from db import db, storage
import config


class Stats(Resource):
    def exec_count_query(self, query):
        rows = db.execute_select(query)
        if len(rows[0]) == 1:
            return rows[0][0]
        else:
            return rows[0]

    def get_media_types_stat(self):
        c = db.execute_select("select fs_media_type, count(*), sum(fs_file_size) from files group by fs_media_type")
        stats = {}
        for mt, count, size in c:
            stats[mt] = {
                "count": count,
                "size": size
            }

        return stats

    def get_trashes(self):
        count, size = self.exec_count_query("select count(*), sum(fs_file_size) from deleted_file_ids left join files on del_fs_id=fs_id")
        return {
            "count": count,
            "size": size
        }

    def get_optimized(self):
        c = db.execute_select("select fs_id, fs_media_type from files")
        nb_optimized = 0
        for id, mt in c:
            opt_filename = storage.optimizedFilename(id, mt)
            if os.path.isfile(opt_filename):
                nb_optimized += 1

        return nb_optimized

    def get_starred(self):
        return self.exec_count_query("select count(*) from starred_file_ids")

    def get_totals(self):
        count, size = self.exec_count_query("select count(*), sum(fs_file_size) from files")
        return {
            "count": count,
            "size": size
        }

    def get_db_size(self):
        return os.path.getsize(config.dbpath)

    def get_anomalies(self):
        c = db.execute_select("select fs_id, fs_dir, fs_filename, fs_file_size from files")
        all_anomalies = {}

        for id, path, filename, file_size in c:
            anomalies = []
            full_path = config.datadir + "/" + path + "/" + filename
            if not os.path.isfile(full_path):
                anomalies.append({
                    "missing_file": path + "/" + filename
                })
            else:
                real_file_size = os.path.getsize(full_path)
                if real_file_size != file_size:
                    anomalies.append({
                        "size_differs": {
                            "real": real_file_size,
                            "db": file_size
                        }
                    })

            if len(anomalies) > 0:
                all_anomalies[id] = anomalies

        return all_anomalies

    def get_all_files(self, root0, root1):
        abs_root = root0 + "/" + root1
        if not os.path.isdir(abs_root):
            return

        for f in os.listdir(abs_root):
            if os.path.isdir(abs_root + "/" + f):
                for sf in self.get_all_files(root0, root1 + "/" + f):
                    yield sf
            else:
                yield root1 + "/" + f

    def get_not_scanned(self):
        all_names = set()
        c = db.execute_select("select fs_dir, fs_filename from files")
        for path, filename in c:
            all_names.add(path + "/" + filename)

        not_scanned = []
        for f in self.get_all_files(config.datadir, ""):
            if f not in all_names:
                not_scanned.append(f)

        return not_scanned

    def get_dir_stat(self, path):
        total_size = 0
        total_count = 0
        for f in self.get_all_files(path, ""):
            total_size += os.path.getsize(path + "/" + f)
            total_count += 1

        return total_count, total_size

    def get_work_dir(self):
        # all_ids = set()
        # c = self.exec_query("select fs_id from files")
        # for id in c:
        #     all_ids.add(id)

        optimized_count, optimized_size = self.get_dir_stat(config.workdir + "/optimized")
        thumbnails_count, thumbnails_size = self.get_dir_stat(config.workdir + "/thumbnails")

        return {
            "optimized": {
                "count": optimized_count,
                "size": optimized_size
            },
            "thumbnails": {
                "count": thumbnails_count,
                "size": thumbnails_size
            }
        }

    def get_storage(self):
        st = os.statvfs(config.datadir)
        return {
            "free": st.f_bavail * st.f_frsize,
            "total": st.f_blocks * st.f_frsize
        }

    def get(self):
        return {
            "total": self.get_totals(),
            "media_types": self.get_media_types_stat(),
            "trashes": self.get_trashes(),
            "optimized": self.get_optimized(),
            "starred": self.get_starred(),
            "db": {
                "file_size": self.get_db_size(),
            },
            # "anomalies": self.get_anomalies(),
            # "not_scanned": self.get_not_scanned(),
            "workdir": self.get_work_dir(),
            "storage": self.get_storage(),
        }
