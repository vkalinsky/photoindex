import typing
from flask import request
from flask_restful import Resource
from dataclasses import dataclass

from sqlite3 import IntegrityError

from db import db

@dataclass
class Album:
    id: str
    name: str
    query: typing.Union[str, None]
    path: str
    stats: dict

    def toJSON(self) -> dict:
        return {
            "id": self.id,
            "name": self.name,
            "query": self.query,
            "path": self.path,
            "stats": self.stats
        }

    def maxDate(self) -> str:
        if self.stats is None:
            return "0"

        return str(self.stats["dates"]["max"])


def get_by_id(album_id: str) -> typing.Union[Album, None]:
    all_albums = get_all_albums()
    return next((a for a in all_albums if str(a.id) == str(album_id)), None)

def get_all_albums() -> typing.List[Album]:
    """
    Returns all albums
    """

    rows = db.execute_select(
        "select \
            ia_album_id as id, \
            min(fs_date_taken) as min_date, \
            max(fs_date_taken) as max_date, \
            count(*) \
            from items_in_albums_v1 \
            left join files on fs_id=ia_fs_id group by ia_album_id;"
    )

    album_stats = {
        str(id): {
            "dates": {
                "min": min_date,
                "max": max_date
            },
            "nb_items": count
        }
        for id, min_date, max_date, count in rows
    }

    rows = db.execute_select("select album_id, album_name, album_query from albums_v1")

    albums = [
        Album(
            id=str(album_id),
            name=album_name,
            path=f".album:{album_id}",
            query=album_query,
            stats=album_stats.get(str(album_id))
        )
        for album_id, album_name, album_query in rows
    ]

    # Return sorted by max date
    return sorted(albums, key=lambda a: a.maxDate(), reverse=True)

class Albums(Resource):
    def get(self, album_id: str = None):
        """
        Returns all albums
        """

        if album_id is not None:
            album = get_by_id(album_id)
            if album is None:
                return {"message": "Album not found"}, 404
            return album.toJSON()

        return [a.toJSON() for a in get_all_albums()]

    def put(self, album_id: str):
        """
        Updates an album

        Request body:
        {
            "name": "Album name"
            "query": "Query"
        }

        Returns 200 OK with body:
        {
            "id": 1,
            "name": "Album name"
            "query": "Query"
        }
        """

        name = request.json["name"]
        query = request.json["query"]

        db.execute_and_commit(
            "update albums_v1 set album_name=:name, album_query=:qry where album_id=:album_id",
            album_id=album_id,
            name=name,
            qry=query)  # qry because the first argument of execute_and_commit is "query"

        album = get_by_id(album_id)
        return album.toJSON(), 200

    def post(self):
        """
        Creates a new album

        Request body:
        {
            "name": "Album name"
        }

        Returns 200 OK with body:
        {
            "id": 1,
            "name": "Album name"
        }
        """

        name = request.json["name"]

        try:
            album_id = db.insert_values("albums_v1", 
            {
                "album_name": name
            })
        except IntegrityError:
            return {"message": "Album with this name already exists"}, 400

        album = get_by_id(album_id)
        return album.toJSON(), 201


    def delete(self, album_id: str):
        """
        Deletes an album
        """

        db.execute_and_commit(
            "delete from albums_v1 where album_id=:album_id",
            album_id=album_id)

        return "OK", 200


class AlbumItem(Resource):
    def put(self, album_id: str):
        """
        Puts item into album
        Endpoint: PUT /api/albums/<album_id>/items
        """

        album = get_by_id(album_id)
        if album is None:
            return "Album does not exist", 404

        items = request.json.get("item_ids", [])
        if not isinstance(items, list) or len(items) == 0:
            return "No items", 400
        
        stritems = ','.join(['?' for it in items])
        query = f"select fs_id from files where fs_id in ({stritems})"
        db_items = db.connect().execute(query, items).fetchall()
        if len(db_items) != len(items):
            items_idx = set(items)
            db_items_idx = set([i[0] for i in db_items])
            missing_items = items_idx - db_items_idx
            return "Items not found: {}".format(",".join(missing_items)), 404

        # Inserting all items at once
        with db.transaction():
            for item_id in items:
                try:
                    db.execute_and_commit(
                        "insert into items_in_albums_v1(ia_album_id, ia_fs_id) values(:album_id, :item_id)",
                        album_id=album_id,
                        item_id=item_id)
                except IntegrityError:
                    pass

        return "OK", 200

    def delete(self, album_id: str, item_id: str):
        """
        Deletes item from album
        """

        r = db.execute_select(
            "select ia_fs_id from items_in_albums_v1 where ia_album_id=:album_id and ia_fs_id=:item_id",
            album_id=album_id,
            item_id=item_id)

        if len(r) == 0:
            return "Not found", 404

        db.execute_and_commit(
            "delete from items_in_albums_v1 where ia_album_id=:album_id and ia_fs_id=:item_id",
            album_id=album_id,
            item_id=item_id)

        return "Deleted", 200
