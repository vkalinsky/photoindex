import math
import random
from flask import request
from flask_restful import Resource
from db import db

import geojson

class Locations(Resource):
    def get(self):
        result = []

        rows = db.execute_select(
            "select \
                fs_id, fs_gps_lon, fs_gps_lat, fs_gps_alt, fs_dir, \
                gt_lat, gt_lon, gt_zoom \
                from files \
                left join geo_tags_v1 on gt_fs_id = fs_id \
                where fs_gps_lon is not null or gt_lon is not null")

        random.seed(0)
        for fs_id, fs_lon, fs_lat, fs_alt, fs_dir, gt_lat, gt_lon, gt_zoom in rows:
            item_props = {
                "id": fs_id,
                "dir": fs_dir,
            }

            # Apply user's geo tag if available
            if gt_lat is not None and gt_lon is not None:
                distance_in_meters_lat = random.random() * 20000 / (math.pow(2, gt_zoom/4))
                distance_in_meters_lon = random.random() * 20000 / (math.pow(2, gt_zoom/4))

                fs_lat = gt_lat + distance_in_meters_lat / 111320
                fs_lon = gt_lon + distance_in_meters_lon / 111320 * math.cos(math.radians(fs_lat))
                item_props["geo_tagged"] = True

            if fs_alt is not None and fs_alt > 1000:
                item_props["plane"] = True

            feature = geojson.Feature(fs_id, geojson.Point((fs_lon, fs_lat)), 
            properties=item_props)
            result.append(feature)
        
        return geojson.FeatureCollection(result)

class NamedLocations(Resource):
    def get(self):
        result = []
        rows = db.execute_select("select nl_id, nl_name, nl_lon, nl_lat, nl_zoom from named_locations_v1")
        for nl_id, nl_name, nl_lon, nl_lat, nl_zoom in rows:
            location = {
                "id": nl_id,
                "name": nl_name,
                "lon": nl_lon,
                "lat": nl_lat,
                "zoom": nl_zoom,
            }

            result.append(location)

        return {"locations": result}
        
    def post(self):
        """
        Request body:
        {
            "name": "name",
            "lon": 0.0,
            "lat": 0.0,
            "zoom": 0
        }
        """
        
        data = request.get_json()
        name = data["name"]
        lon = data["lon"]
        lat = data["lat"]
        zoom = data["zoom"]

        if name is None or lon is None or lat is None or zoom is None:
            return "Missing data", 400
        
        inserted_id = db.insert_values("named_locations_v1", {
            "nl_name": name,
            "nl_lon": lon,
            "nl_lat": lat,
            "nl_zoom": zoom,
        })

        return {
            "id": inserted_id,
            "name": name,
            "lon": lon,
            "lat": lat,
            "zoom": zoom,
            }, 201

class NamedLocation(Resource):
    def delete(self, location_id):
        db.delete("named_locations_v1", "nl_id", location_id)
        return 200, "deleted"
        