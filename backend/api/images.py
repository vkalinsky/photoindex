import os
import mimetypes
import datetime
import json
import subprocess

from flask import request, send_file
from flask_restful import Resource
from PIL import Image

import config
from db import db, storage
from . import utils

mimetypes.init()


class Images(Resource):
    def get(self, item_id):
        item_info = db.fetchItemInfo(item_id, ["fs_dir", "fs_filename", "fs_orientation", "fs_media_type"])

        if item_info is None:
            return ({"Item with this id does not exist"}, 404)

        dir, filename, orientation, media_type = item_info

        forceOriginal = int(request.args.get("forceOriginal", 0)) == 1
        optimized = storage.optimizedFilename(item_id, media_type) \
            if not forceOriginal \
            else None

        if optimized is None or not os.path.isfile(optimized):
            fullpath = config.datadir + "/" + dir + "/" + filename
        else:
            fullpath = optimized

        if not os.path.isfile(fullpath):
            return ({"File does not exist"}, 404)

        if media_type == "image" and orientation is not None and orientation != 1:
            with open(fullpath, "rb") as f:
                with utils.benchmark("Unrotating image: %d" % orientation):
                    im = Image.open(f)
                    im = utils.rotate_pil_image(im, orientation)
                return utils.serve_pil_image(im)

        download = int(request.args.get("download", 0)) == 1

        mm = mimetypes.MimeTypes()
        mmt = mm.guess_type(filename)
        mimetype = mmt[0]

        return send_file(fullpath, mimetype=mimetype, as_attachment=download, conditional=True)

    def delete(self, item_id):
        if not db.item_exists(item_id):
            return ("Item with this id does not exist", 404)

        db.insert_values("deleted_file_ids", {
            "del_fs_id": item_id,
            "del_fs_timestamp": datetime.datetime.now().isoformat()
        })
        return "Removed"

    def post(self, item_id):
        if "action" not in request.json:
            return "Invalid request", 400

        item_info = db.fetchItemInfo(item_id, ["fs_id"])
        if item_info is None:
            return ("Item with this id does not exist", 404)

        action = request.json["action"]

        if action == "star":
            query = "insert into starred_file_ids values(:item_id, :date)"
            expected_state = True
        elif action == "unstar":
            expected_state = False
            query = "delete from starred_file_ids where star_fs_id = :item_id and '1' != :date"
            

        if db.isItemStarred(item_id) != expected_state:
            now_str = datetime.datetime.now().isoformat()
            db.execute_and_commit(query, item_id=item_id, date=now_str)

        return {"starred": expected_state}

class ImageTask(Resource):
    def post(self, item_id, task):
        if not db.item_exists(item_id):
            return ("Item with this id does not exist", 404)

        from tasks import generate_thumbnail, optimize, generate_filmstrip, find_faces, fix_timestamp

        task_fn = {
            "generate_thumbnail": generate_thumbnail,
            "optimize": optimize,
            "generate_filmstrip": generate_filmstrip,
            "find_faces": find_faces,
            "fix_timestamp": fix_timestamp,
        }.get(task)

        if task_fn is None:
            return ("Unknown task", 400)

        task_fn.apply_async((item_id,))

        return "OK"

class ImageDetails(Resource):
    def ffprobe_file(self, path):
        cmdline = [
            'ffprobe',
            '-hide_banner', '-v', 'quiet',
            '-show_streams',
            '-show_format',
            '-print_format', 'json',
            path]

        p = subprocess.Popen(cmdline, stdout=subprocess.PIPE, shell=False)
        res = p.wait()
        if res != 0:
            return None

        file_metadata_str = p.stdout.read()
        file_metadata = json.loads(file_metadata_str)
        return file_metadata

    def get_optimized_info(self, item_id, full_path, media_type):
        optimizedFilename = storage.optimizedFilename(item_id, media_type)
        if not os.path.isfile(optimizedFilename):
            return None

        originalSize = os.path.getsize(full_path)
        optimizedSize = os.path.getsize(optimizedFilename)
        ratio = originalSize / (optimizedSize + 1)

        return {
            "size": optimizedSize,
            "ratio": ratio,
            "ffprobe": self.ffprobe_file(optimizedFilename)
        }

    def get(self, item_id):
        details = {}
        dbkeys = [
            # I'm too lazy now to make a dict, so item[n].
            "fs_id",          # item[0]
            "fs_filename",    # item[1]
            "fs_dir",         # item[2]
            "fs_media_type",  # item[3]
            "fs_file_size",
            "fs_date_taken",
            "fs_date_taken_local",
            "fs_gps_lon",
            "fs_gps_lat",
            "fs_gps_alt",
            "fs_duration",
            "fs_frame_width",
            "fs_frame_height",
            "fs_orientation",
            "fs_date_import"
        ]

        item = db.fetchItemInfo(item_id, dbkeys)

        if item is None:
            return ({"Item with this id does not exist"}, 404)

        details["db"] = {}
        for i in range(len(dbkeys)):
            details["db"][dbkeys[i]] = item[i]

        fullpath = config.datadir + '/' + item[2] + '/' + item[1]
        details["fullpath"] = fullpath
        details["ffprobe"] = self.ffprobe_file(fullpath)

        optimizedInfo = self.get_optimized_info(item_id, fullpath, item[3])
        if optimizedInfo is not None:
            details["optimized"] = optimizedInfo

        imf = ImageFaces()
        faces = imf.get(item_id)
        if faces is not None:
            details["faces"] = faces

        return details


class ImageFaces(Resource):
    def __init__(self):
        self._cols = [
            "fip_id",
            "fip_top",
            "fip_left",
            "fip_right",
            "fip_bottom",
            "fip_person_id",
            "fip_person_id_guess"
        ]
        self._str_cols = ','.join(self._cols)

    def get(self, item_id):
        result = []
        rows = db.execute_select("select %s from faces_in_pictures where fip_fs_id=:item_id" % self._str_cols, item_id=item_id)
        for row in rows:
            face = {}
            for k, v in dict(zip(self._cols, list(row))).items():
                face[k.replace("fip_", "")] = v

            if face["top"] is not None:  # fields are NULL if faces not found
                result.append(face)

        return result

class ImageLocation(Resource):
    def patch(self, item_id):
        data = request.json
        lat = data.get("lat")
        lon = data.get("lon")
        zoom = data.get("zoom")
        if lat is None or lon is None or zoom is None:
            return "Invalid request", 400

        items = [item_id]
        if item_id == "_batch":
            items = data.get("items")
            if items is None:
                return "Invalid request", 400

        conn = db.connect()
        conn.execute("begin")
        try:
            for item in items:
                db.execute_and_commit(
                    "insert into geo_tags_v1(gt_fs_id, gt_lat, gt_lon, gt_zoom) values(:item_id, :lat, :lon, :zoom)ON CONFLICT(gt_fs_id) DO UPDATE SET gt_lat = :lat, gt_lon = :lon, gt_zoom = :zoom WHERE gt_fs_id = :item_id",
                    item_id=item,
                    lat=lat,
                    lon=lon,
                    zoom=zoom)

            conn.execute("commit")
        except Exception as e:
            conn.execute("rollback")
            raise e

        return "OK"


# Files upload

class Upload(Resource):
    def post(self):
        if "file" not in request.files:
            return "Invalid request", 400

        file = request.files["file"]
        if file.filename == "":
            return "Invalid request", 400

        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(config.datadir, filename))
            return "OK"
