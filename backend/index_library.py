#!/usr/bin/env python3

import os
import argparse
import config
import time

from db import db, utility
import tasks


class Scanner:
    def __init__(self, datadir, dbpath, dryrun=False):
        if not os.path.isdir(datadir):
            raise Exception("Directory %s does not exist" % datadir)

        self._datadir = datadir
        self._dryrun = dryrun
        self._dirs = []
        self._files = []
        self._fileIds = {}

        if not self._dryrun:
            if not os.path.isfile(dbpath):
                raise Exception("Database file %s does not exist" % dbpath)

            self.build_index()

    def build_index(self):
        for fs_id, fs_dir in db.execute_select("select fs_id, fs_dir from files"):
            self._fileIds[fs_id] = fs_dir

    def scan_dir(self, dirname):
        print("Scanning %s" % dirname)

        for f in os.listdir(self._datadir + dirname):
            if f[0] == '.':
                continue

            full_path = '/'.join([config.datadir, dirname, f])
            rel_path = dirname + '/' + f
            if os.path.isdir(full_path):
                self.scan_dir(rel_path)
            else:
                file_id = utility.getFileIdForPath(full_path)
                if file_id in self._fileIds and self._fileIds[file_id] == dirname:
                    continue

                print("Importing", rel_path)
                if not self._dryrun:
                    tasks.import_file.apply_async((rel_path,), priority=9)

                self._fileIds[file_id] = dirname

    def scan(self):
        self.scan_dir("")
        print("Scanning complete")


def main():
    parser = argparse.ArgumentParser(
        description='Sort jpegs and videos in given directory')
    parser.add_argument('--dry', action="store_true")
    args = parser.parse_args()

    scanner = Scanner(config.datadir, config.dbpath, dryrun=args.dry)
    while True:
        try:
            scanner.scan()
        except Exception as e:
            print(e)

        if args.dry:
            break

        time.sleep(120)


if __name__ == '__main__':
    main()
