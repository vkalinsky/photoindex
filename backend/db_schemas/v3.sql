BEGIN TRANSACTION;

PRAGMA user_version=3;

CREATE TABLE files_tmp(fs_id TEXT PRIMARY KEY, fs_filename TEXT, fs_dir TEXT, fs_file_size INTEGER, fs_date_taken TEXT, fs_gps_lon REAL, fs_gps_lat REAL, fs_frame_width INTEGER, fs_frame_height INTEGER, fs_media_type TEXT, fs_orientation INTEGER, fs_gps_alt REAL, fs_duration REAL, fs_date_import DATE DEFAULT (datetime('now')));

INSERT INTO files_tmp(fs_id, fs_filename, fs_dir, fs_file_size, fs_date_taken, fs_gps_lon, fs_gps_lat, fs_frame_width, fs_frame_height, fs_media_type, fs_orientation, fs_gps_alt, fs_duration, fs_date_import) SELECT fs_id, fs_filename, fs_dir, fs_file_size, fs_date_taken, fs_gps_lon, fs_gps_lat, fs_frame_width, fs_frame_height, fs_media_type, fs_orientation, fs_gps_alt, fs_duration, fs_date_taken FROM files;

DROP TABLE files;
ALTER TABLE files_tmp RENAME TO files;

COMMIT TRANSACTION;

