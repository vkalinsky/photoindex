BEGIN TRANSACTION;

PRAGMA user_version=2;


-- Migration from v0 to v2

CREATE TABLE faces_in_pictures_tmp(fip_id INTEGER PRIMARY KEY AUTOINCREMENT, fip_fs_id INTEGER NOT NULL, fip_top REAL, fip_left REAL, fip_right REAL, fip_bottom REAL, fip_encodings TEXT, fip_landmarks TEXT, fip_person_id INTEGER, fip_person_id_guess INTEGER, fip_ignored INTEGER default 0);

INSERT INTO faces_in_pictures_tmp(fip_id, fip_fs_id, fip_top, fip_left, fip_right, fip_bottom, fip_encodings, fip_landmarks) SELECT fip_id, fip_fs_id, fip_top, fip_left, fip_right, fip_bottom, fip_encodings, fip_landmarks FROM faces_in_pictures;

DROP TABLE faces_in_pictures;
ALTER TABLE faces_in_pictures_tmp RENAME TO faces_in_pictures;

CREATE TABLE files_tmp(fs_id TEXT PRIMARY KEY NOT NULL, fs_filename TEXT NOT NULL, fs_dir TEXT NOT NULL, fs_file_size INTEGER NOT NULL, fs_date_taken TEXT, fs_gps_lon REAL, fs_gps_lat REAL, fs_gps_alt REAL, fs_duration REAL, fs_frame_width INTEGER NOT NULL, fs_frame_height INTEGER NOT NULL, fs_media_type TEXT NOT NULL, fs_orientation INTEGER DEFAULT 1);

INSERT INTO files_tmp(fs_id, fs_filename, fs_dir, fs_file_size, fs_date_taken, fs_gps_lon, fs_gps_lat, fs_frame_width, fs_frame_height, fs_media_type, fs_orientation, fs_gps_alt, fs_duration) SELECT fs_id, fs_filename, fs_dir, fs_file_size, fs_date_taken, fs_gps_lon, fs_gps_lat, fs_frame_width, fs_frame_height, fs_media_type, fs_orientation, fs_gps_alt, fs_duration FROM files;

DROP TABLE files;
ALTER TABLE files_tmp RENAME TO files;


-- Re-create indices after migration

CREATE INDEX idx_files_to_faces_in_pictures on faces_in_pictures(fip_fs_id);
CREATE INDEX idx_people_to_faces_in_pictures on faces_in_pictures(fip_person_id);
CREATE INDEX idx_people_to_faces_in_pictures_guess on faces_in_pictures(fip_person_id_guess);
CREATE UNIQUE INDEX idx_unique_people on people(person_name, person_dob);

COMMIT TRANSACTION;
