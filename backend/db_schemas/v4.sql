BEGIN TRANSACTION;

PRAGMA user_version=4;

CREATE TABLE named_locations_v1(nl_id INTEGER PRIMARY KEY, nl_name TEXT NOT NULL UNIQUE, nl_lon REAL, nl_lat REAL, nl_zoom REAL);
CREATE TABLE geo_tags_v1(gt_fs_id TEXT PRIMARY KEY, gt_lon REAL, gt_lat REAL, gt_zoom REAL);

COMMIT TRANSACTION;
