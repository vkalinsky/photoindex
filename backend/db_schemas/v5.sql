BEGIN TRANSACTION;

PRAGMA user_version=5;

CREATE TABLE albums_v1(album_id INTEGER PRIMARY KEY AUTOINCREMENT, album_name TEXT NOT NULL, album_query TEXT);

CREATE TABLE items_in_albums_v1(ia_album_id INTEGER, ia_fs_id TEXT, ia_date_added INTEGER);

CREATE UNIQUE INDEX idx_unique_item_in_album on items_in_albums_v1(ia_album_id, ia_fs_id);

CREATE INDEX idx_album_items on items_in_albums_v1(ia_fs_id);

COMMIT TRANSACTION;
