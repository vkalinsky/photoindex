CREATE TABLE files(fs_id TEXT PRIMARY KEY, fs_filename TEXT, fs_dir TEXT, fs_file_size INTEGER, fs_date_taken TEXT, fs_gps_lon REAL, fs_gps_lat REAL, fs_frame_width INTEGER, fs_frame_height INTEGER, fs_media_type TEXT, fs_orientation INTEGER, fs_gps_alt REAL, fs_duration REAL);

CREATE TABLE deleted_file_ids(del_fs_id TEXT PRIMARY KEY NOT NULL, del_fs_timestamp TEXT);
CREATE TABLE starred_file_ids(star_fs_id TEXT PRIMARY KEY NOT NULL, star_fs_timestamp TEXT);
CREATE TABLE people(person_id INTEGER PRIMARY KEY, person_name TEXT NOT NULL, person_dob TEXT);
CREATE TABLE faces_in_pictures(fip_id INTEGER PRIMARY KEY AUTOINCREMENT, fip_fs_id INTEGER NOT NULL, fip_top REAL, fip_left REAL, fip_right REAL, fip_bottom REAL, fip_encodings TEXT, fip_landmarks TEXT, person_id INTEGER);
