BEGIN TRANSACTION;

PRAGMA user_version=6;

-- This fields are calculated from fs_date_taken, location info and neighboring files.
-- We will try our best to find the correct date taken for each file.
ALTER TABLE files ADD COLUMN fs_date_taken_local INTEGER NULL;

COMMIT TRANSACTION;
