#!/usr/bin/env python

from flask import Flask
from flask_restful import Api
from flask_cors import CORS
import argparse
import config
from db import db
from api import albums, directories, images, locations, thumbnails, faces, people, stats, tasks


app = Flask(__name__)
CORS(app)
api_app = Api(app)

parser = argparse.ArgumentParser(description='API server')
parser.add_argument('--host', action="store", default=config.host)
parser.add_argument('--port', action="store", default=5000)
parser.add_argument('--debug', action="store_true")
args = parser.parse_args()

api_app.add_resource(thumbnails.Thumbnails, '/api/thumbnails/<string:item_id>')
api_app.add_resource(thumbnails.Filmstrips, '/api/filmstrips/<string:item_id>')

api_app.add_resource(images.Images, '/api/images/<string:item_id>')
api_app.add_resource(images.ImageTask, '/api/images/<string:item_id>/task/<string:task>')
api_app.add_resource(images.ImageDetails, '/api/images/<string:item_id>/details')
api_app.add_resource(images.ImageFaces, '/api/images/<string:item_id>/faces')
api_app.add_resource(images.ImageLocation, '/api/images/<string:item_id>/location')

api_app.add_resource(locations.Locations, '/api/locations')
api_app.add_resource(locations.NamedLocations, '/api/locations/named')
api_app.add_resource(locations.NamedLocation, '/api/locations/named/<string:location_id>')

api_app.add_resource(faces.GetBatchOfUntagged, '/api/faces/untagged')
api_app.add_resource(faces.ImageFacesSingle, '/api/faces/<int:fip_id>', '/api/faces/batch')

api_app.add_resource(faces.ImageFacesThumbnails, '/api/faces/<int:fip_id>/thumbnail')
api_app.add_resource(faces.GetFaceContext, '/api/faces/<int:fip_id>/context')
api_app.add_resource(faces.FindSimilar, '/api/faces/<int:fip_id>/similar')

api_app.add_resource(directories.Directories, '/api/dirs/')
api_app.add_resource(directories.ItemsInDir, '/api/dirs/<string:path>/items')

api_app.add_resource(people.People, '/api/people')
api_app.add_resource(people.Person, '/api/people/<int:person_id>')

api_app.add_resource(albums.Albums, '/api/albums', '/api/albums/<int:album_id>')
api_app.add_resource(albums.AlbumItem, '/api/albums/<int:album_id>/items/<string:item_id>', '/api/albums/<int:album_id>/items')

api_app.add_resource(tasks.Tasks, "/api/tasks")
api_app.add_resource(stats.Stats, "/api/stats")

if __name__ == '__main__':
    db_ver = db.get_db_version()
    if config.database_version != db_ver:
        raise Exception("DB version mismatched, current %d, required %d" % (db_ver, config.database_version))

    app.run(debug=args.debug, host=args.host, port=args.port)
