import os
import hashlib


def getFileId(filename, file_size):
    md5 = hashlib.md5()
    md5.update(os.path.basename(filename).encode('utf-8'))
    return "%s_%d" % (md5.hexdigest(), file_size)


def getFileIdForPath(path):
    return getFileId(path, os.path.getsize(path))


def transformFrameSize(width, height, orientation):
    """
    Makes new frame with respect to orientation

    width, height: int
        Dimensions of the frame

    orientation: int
        Numeric orientation id

    Returns: tuple
        (width, height)
    """

    if orientation in [5, 6, 7, 8]:
        return height, width
    else:
        return width, height


def makedirs(filename):
    """
    Makes dirname(filename) recursively if needed

    Returns: None
    """
    dir = os.path.dirname(filename)
    if not os.path.isdir(dir):
        os.makedirs(dir)
