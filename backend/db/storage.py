from . import db
import config


def getOriginalFilename(item_id):
    item_info = db.fetchItemInfo(item_id, ["fs_dir", "fs_filename"])
    return '/'.join([config.datadir, item_info[0], item_info[1]])


def optimizedFilename(item_id, media_type):
    """
    Full path to optimized file

    item_id: str
        Id of the item

    media_type: str
        "video" or "audio"

    Returns: str
        Full path to optimized file

    """
    if media_type == "image":
        ext = "jpg"
    elif media_type == "video":
        ext = "mp4"

    return config.workdir + "/optimized/%s/%s/%s.%s" % (
        item_id[0],
        item_id[1],
        item_id,
        ext)


def thumbnailFilename(item_id):
    """
    Full path to optimized file

    item_id: str
        Id of the item

    Returns: str
        Full path to optimized file

    """

    return config.workdir + "/thumbnails/%s/%s/%s.jpg" % (
        item_id[0],
        item_id[1],
        item_id)


def filmstripFilename(item_id):
    """
    Full path to film strip file

    item_id: str
        Id of the item

    Returns: str
        Full path to film strip file

    """
    
    return config.workdir + "/filmstrips/%s/%s/%s.jpg" % (
        item_id[0],
        item_id[1],
        item_id)


def facepreviewFilename(fip_id):
    """
    Full path to face preview file

    fip_id: str
        Face id (fip_id)

    Returns: str
        Full path to the jpeg file

    """

    return config.workdir + "/facepreviews/face_{}.jpg".format(fip_id)
