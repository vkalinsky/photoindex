import sqlite3
import config

class TransactionContext:
    def __init__(self):
        self.db = connect()

    def __enter__(self):
        self.db.execute("begin")
        return self

    def __exit__(self, tp, val, tb):
        if tp is None:
            self.db.execute("commit")
        else:
            self.db.execute("rollback")

        self.db.close()

def connect():
    db = sqlite3.connect(config.dbpath)
    db.text_factory = str
    return db

def get_db_version():
    (r,) = connect().execute("pragma user_version").fetchone()
    return r

def transaction():
    return TransactionContext()

def execute_select(query, **kwargs):
    """
    Executes a select query

    query: str
        Query to execute. If not select, raises exception
        Queries are parametrized by :param, passed in kwargs:
         In : execute_select("select :value * 5", value=12)
         Out: [(60,)]

    Returns:
        List of rows
    """
    if query[:6].lower() != 'select':
        raise Exception("Query '%s' is not select" % query)

    return connect().execute(query, kwargs).fetchall()

def execute_and_commit(query, **kwargs):
    """
    Executes a query

    query: str

    Query to execute. If not select, raises exception
        Queries are parametrized by :param, passed in kwargs:
         In : execute_select("select :value * 5", value=12)
    """
    db = connect()
    db.execute(query, kwargs).fetchall()
    db.commit()


def insert_values(table: str, values: dict) -> int:
    """
    Creates and executes an insert query

    table: str
        Table name

    values: dict
        field_name => field_value dictionary

    Returns: int
        id of the inserted row

    Raises if request failed
    """

    fields = ','.join(values.keys())
    placeholder = ','.join(["?"] * len(values))
    query = "insert into %s(%s) values(%s)" % (table, fields, placeholder)

    db = connect()
    c = db.cursor()
    try:
        c.execute(query, tuple(values.values()))
        lastrowid = c.lastrowid
        db.commit()

        return lastrowid
    except Exception as e:
        db.rollback()
        raise e

def delete(table, field, value):
    """
    Deletes a row from a table

    table: str
        Table name

    field: str
        Field name

    value: str
        Field value
    """

    db = connect()
    db.execute("delete from %s where %s=?" % (table, field), [value])
    db.commit()

def item_exists(item_id):
    """
    Returns True if item exists, False otherwise
    """

    db = connect()
    rows = db.execute("select fs_id from files where fs_id=?", [item_id]).fetchmany(1)
    return len(rows) > 0


def fetchItemInfo(item_id, fields):
    """
    Gets specified fields for a single item

    item_id: string
        id of the item

    fields: list
        db fields (fs_id, fs_name, etc) or ['*'] (please don't)

    Returns:
        A tuple with field values

    Raises:
        Exception if item is not found

    """

    strFields = ', '.join(fields)
    rows = connect().execute("select %s from files where fs_id=?" % strFields, [item_id]).fetchmany(1)

    if len(rows) == 0:
        raise Exception("Item %s not found" % item_id)
    else:
        return rows[0]


def isItemStarred(item_id):
    rows = connect().execute("select count(*) from starred_file_ids where star_fs_id=?", [item_id]).fetchmany(1)
    return rows[0][0] == 1
