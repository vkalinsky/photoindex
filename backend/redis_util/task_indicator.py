from dataclasses import dataclass
import time
import uuid
import typing
import dataclasses
from .connect import connect

@dataclass
class Task():
    item_id: str
    name: str
    created_at: float
    ttl: int = 86400*7
    status: str = "pending"

    def __post_init__(self):
        self._id = "task_" + self.item_id + "_" + self.name

    def get_all_tasks() -> typing.Dict[str, "Task"]:
        rc = connect()
        keys = rc.keys("task_*")
        tasks = {}
        for key in keys:
            task_data = rc.hgetall(key)
            task = Task(**task_data)
            tasks[key] = task

        return tasks

    def get_item_tasks(item_id: str) -> typing.List["Task"]:
        rc = connect()
        keys = rc.keys("task_" + item_id + "_*")
        tasks = []
        for key in keys:
            task_data = rc.hgetall(key)
            task = Task(**task_data)
            tasks.append(task)

        return tasks

    def write_to_redis(self):
        rc = connect()
        rc.hset(self._id, mapping=dataclasses.asdict(self))
        rc.expire(self._id, self.ttl)

    def mark_done(self):
        self.ttl = 3600
        self.status = "done"
        self.write_to_redis()

    def to_dict(self) -> typing.Dict[str, typing.Any]:
        return dataclasses.asdict(self)


class TaskContext():
    def __init__(self, item_id: str, task_name: str, ttl: int = 60):
        self._rc = connect()
        self._task = Task(item_id, task_name, time.time(), ttl)
        self._ttl = ttl

    def __enter__(self):
        self._task.status = "running"
        self._task.write_to_redis()
        return self

    def __exit__(self, tp, val, tb):
        self._task.mark_done()
