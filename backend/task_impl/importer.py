import typing
import os
import datetime

from media_handlers.jpeg_handler import JPEGHandler
from media_handlers.png_handler import PNGHandler
from media_handlers.movie_handler import MovieHandler
from media_handlers.base import DBKeys
from db import db, utility
import config

_handlers = {
    "jpeg": JPEGHandler,
    "jpg": JPEGHandler,
    "mov": MovieHandler,
    "mp4": MovieHandler,
    "avi": MovieHandler,
    "png": PNGHandler,
}

def _insert_file(metadata):
    values = {}
    for it in metadata.items():
        values["fs_" + it[0]] = str(it[1])

    db.insert_values("files", values)


def validate_metadata(metadata):
    if metadata is None:
        raise Exception("Metadata is empty")

    w = metadata.get(DBKeys.DB_KEY_FRAME_WIDTH, None)
    h = metadata.get(DBKeys.DB_KEY_FRAME_HEIGHT, None)
    if w is None or h is None:
        raise Exception("No frame size")

    # This field can be empty for files sent via mail or IM.
    # if metadata.get(DBKeys.DB_KEY_DATE_TAKEN, None) is None:
    #     raise Exception("No date time")

def convert_to_utc(datetime_str: str) -> datetime.datetime:
    try:
        # Parse the datetime string
        dt = datetime.datetime.fromisoformat(datetime_str)
        
        # Check if the datetime object has timezone info
        if dt.tzinfo is None:
            # Assume it's already in UTC if no timezone info is provided
            return dt
        else:
            # Convert to UTC
            return dt.astimezone(datetime.timezone.utc)
    except ValueError as e:
        raise ValueError(f"Invalid ISO datetime format: {datetime_str}") from e

def _get_file_metadata(path: str) -> dict:
    """
    Get metadata for a file
    """
    ext = os.path.splitext(path.lower())[1][1:]

    if ext not in _handlers:
        raise Exception("Unknown type %s" % ext)

    full_path = config.datadir + '/' + path
    if not os.path.isfile(full_path):
        raise Exception("No such file: %s" % full_path)

    handler = _handlers[ext](full_path)

    metadata = handler.get_metadata()

    validate_metadata(metadata)

    metadata["dir"] = os.path.dirname(path)
    metadata["filename"] = os.path.basename(path)

    file_size = os.path.getsize(full_path)
    item_id = utility.getFileId(path, file_size)
    metadata["file_size"] = file_size
    metadata["id"] = item_id

    # Metadata comes with DBKeys.DB_KEY_DATE_TAKEN_LOCAL that's set to local timestamp (with timezone).
    # We need to convert it to UTC timezone and set DBKeys.DB_KEY_DATE_TAKEN to it.
    metadata[DBKeys.DB_KEY_DATE_TAKEN] = convert_to_utc(metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL]).isoformat()

    return metadata

def import_file(path) -> typing.Union[dict, None]:
    """
    Imports a file into the database,
    returns entry info if file was imported, None otherwise
    """
    ext = os.path.splitext(path.lower())[1][1:]

    if ext not in _handlers:
        raise Exception("Unknown type %s" % ext)

    full_path = config.datadir + '/' + path
    if not os.path.isfile(full_path):
        raise Exception("No such file: %s" % full_path)

    fs_dir = None
    file_size = os.path.getsize(full_path)
    item_id = utility.getFileId(path, file_size)

    try:
        fs_dir, fs_filename = db.fetchItemInfo(item_id, fields=["fs_dir", "fs_filename"])
    except:
        pass # Item does not exist

    if fs_dir is not None:
        print(f"File {path} already exists in the database as {fs_dir}/{fs_filename}")
        newDir = os.path.dirname(path)
        if fs_filename != os.path.basename(path):
            raise Exception(f"item id collision: {item_id} {path} {fs_dir} {fs_filename}")

        if fs_dir != newDir:
            # Check if the DB entry points at existing file 
            oldPath = config.datadir + '/' + fs_dir + '/' + fs_filename
            if os.path.isfile(oldPath):
                # We have a duplicate, print log and skip it.
                print(f"Duplicate file: {path}, existing one {oldPath}")
                return None

            # File that is already in DB moved, set new dir for existing item
            print(f"Updating directory for {path} from {fs_dir} to {newDir}")
            db.execute_and_commit("update files set fs_dir = :dir where fs_id = :item_id", dir=newDir, item_id=item_id)

        return None
    

    print("Importing file", full_path)

    metadata = _get_file_metadata(path)

    _insert_file(metadata)

    print("File %s imported as %s" % (full_path, item_id))

    return metadata

def fix_timestamp(item_id: str):
    r = db.fetchItemInfo(item_id, fields=["fs_dir", "fs_filename", "fs_date_taken", "fs_date_taken_local"])
    if r is None:
        raise Exception(f"Item {item_id} not found")
    
    db_dir, db_filename, db_date_taken, db_date_taken_local = r
    
    metadata = _get_file_metadata(db_dir + '/' + db_filename)

    need_to_update = False

    if db_date_taken_local != metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL]:
        need_to_update = True
        print(f"Updating date_taken_local for {item_id}: {db_date_taken_local} -> {metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL]}")

    if db_date_taken != metadata[DBKeys.DB_KEY_DATE_TAKEN]:
        need_to_update = True
        print(f"Updating date_taken for {item_id}: {db_date_taken} -> {metadata[DBKeys.DB_KEY_DATE_TAKEN]}")

    if not need_to_update:
        print(f"Dates are correct for {item_id}")
        return

    db.execute_and_commit("update files set fs_date_taken = :date_taken, fs_date_taken_local = :date_taken_local where fs_id = :item_id",
                            date_taken=metadata[DBKeys.DB_KEY_DATE_TAKEN],
                            date_taken_local=metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL],
                            item_id=item_id)
