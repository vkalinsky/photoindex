import os
import json
import subprocess
from db import db, storage
import config

def _get_number_of_frames(filename: str) -> int:
    """Get the number of frames in a video file.

    filename: str
        Full path to the video file

    Result: int
        Number of frames in the video file

    """

    cmd = [
        "ffprobe",
        "-v", "error",
        "-print_format", "json",
        "-show_streams",
        "-select_streams", "v:0",
        filename,
    ]

    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    if result.returncode != 0:
        raise Exception("ffprobe failed")

    data = json.loads(result.stdout)

    if "streams" not in data:
        raise Exception("No video streams found")

    if len(data["streams"]) == 0:
        raise Exception("No video streams found")

    if "nb_frames" not in data["streams"][0]:
        raise Exception("Unable to get number of frames")
    
    return int(data["streams"][0]["nb_frames"])


def _generate_filmstrip(src_filename: str, dst_filename: str, stride: int) -> None:
    """Generate a film strip from a video file.

    src_filename: str
        Full path to the source video file

    dst_filename: str
        Full path to the destination film strip file

    stride: int
        Number of frames to skip between each frame

    """

    filters = [
        f"select='not(mod(n,{stride}))'",
        f"scale={config.filmstrip_size}:-1",
        "tile=100x1",
        "unsharp=luma_msize_x=3:luma_msize_y=3:luma_amount=1"
    ]

    dst_filename_tmp = dst_filename + ".tmp"
    cmd = [
        "ffmpeg",
        '-hide_banner',
        "-nostdin",
        "-i", src_filename,
        "-vf", ','.join(filters),
        "-vcodec", "mjpeg",
        "-q:v", "2",
        "-frames", "1",
        "-f", "image2",
        "-y",
        dst_filename_tmp,
    ]

    result = subprocess.run(cmd)
    if result.returncode != 0:
        raise Exception("ffmpeg failed")

    os.rename(dst_filename_tmp, dst_filename)


def create_film_strip(item_id: str) -> bool:
    """Create a film strip for a video item.

    item_id: str
        Item id of the item

    Result: boolean
        True if film strip was created, False otherwise

    """

    # Bail right away if the film strip already exists
    dst_filename = storage.filmstripFilename(item_id)
    if os.path.exists(dst_filename):
        print("Film strip for %s already exists" % item_id)
        return False


    # Sanity check first

    fields = [
        "fs_id",
        "fs_dir",
        "fs_media_type",
    ]

    item = db.fetchItemInfo(item_id, fields)

    if item is None:
        raise Exception("Item %s not found" % item_id)

    item = dict(zip(fields, item))

    if item["fs_media_type"] != "video":
        raise Exception("Item %s is not a video" % item_id)

    src_filename = storage.getOriginalFilename(item_id)
    if not os.path.isfile(src_filename):
        raise Exception("File %s not found" % src_filename)


    # Creating the strip

    os.makedirs(os.path.dirname(dst_filename), exist_ok=True)

    nb_frames = _get_number_of_frames(src_filename)
    if nb_frames < 100:
        return False  # Not enough frames to create a strip

    stride = nb_frames // 100

    _generate_filmstrip(src_filename, dst_filename, stride)

    return True