import os
import subprocess

from db import db, storage, utility
import config

def clip(value, lower, upper):
    return lower if value < lower else upper if value > upper else value

def optimize(item_id, rewrite=False):
    """
    Optimizes an item

    item_id: str
        Id of the item

    rewrite: boolean
        Overwrites the existing optimized version if True

    Returns: boolean
        True if optimized, False if not

    Raises:
        Exception if item not found or conversion failed
    """
    fields = [
        "fs_id",
        "fs_dir",
        "fs_filename",
        "fs_media_type",
        "fs_frame_width",
        "fs_frame_height",
        "fs_orientation"
    ]

    id, dir, filename, media_type, iw, ih, orientation = db.fetchItemInfo(item_id, fields)

    src_filename = '/'.join([config.datadir, dir, filename])

    if not os.path.isfile(src_filename):
        print("File %s does not exist" % src_filename)
        return False

    print("Processing %s, %s" % (item_id, src_filename))

    opt_filename = storage.optimizedFilename(id, media_type)
    if os.path.isfile(opt_filename):
        if rewrite:
            print("Optimized file %s exists, rewriting" % opt_filename)
        else:
            print("Optimized file %s exists, skipping" % opt_filename)
            return False

    if media_type == "video":
        iw, ih = utility.transformFrameSize(iw, ih, orientation)

    if iw > 1280 or iw < 320:
        ow = clip(iw, 320, 1280)
        oh = int(ih * ow / iw)
        oh = oh & ~1
    else:
        ow, oh = iw, ih

    utility.makedirs(opt_filename)
    opt_filename_tmp = opt_filename + '.tmp'

    if media_type == "video":
        ffmpeg_args = [
            '-i', src_filename,
            '-vcodec', 'libx264',
            '-preset', 'slow',
            '-movflags', 'faststart',
            '-pix_fmt', 'yuvj420p',
            '-vf', "scale=%dx%d" % (ow, oh),
            '-acodec', 'aac',
            '-strict', '-2',
            '-ar', '44100',
            '-ac', '1',
            '-r', '30',
            '-f', 'mp4',
            opt_filename_tmp
        ]
    elif media_type == "image":
        ffmpeg_args = [
            '-i', src_filename,
            '-vcodec', 'mjpeg',
            '-vf', "scale=%dx%d" % (ow, oh),
            '-f', 'image2',
            '-frames', '1',
            opt_filename_tmp
        ]

    ffmpeg_cmdline = ['ffmpeg', '-hide_banner', '-v', 'error', '-nostdin', '-y'] + ffmpeg_args

    try:
        subprocess.check_call(ffmpeg_cmdline)
    except Exception:
        raise Exception("Could not optimize %s, cmdline %s" % (item_id, ' '.join(ffmpeg_cmdline)))

    os.rename(opt_filename_tmp, opt_filename)
    return True
