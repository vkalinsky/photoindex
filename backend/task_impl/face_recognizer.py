import json
import numpy as np
import config
from db import db


class FaceRecognizer:
    def __init__(self):
        self.get_all_people()
        self.get_all_encodings()

    def get_all_people(self):
        all_people = {}
        rows = db.execute_select("select person_id, person_name from people")
        for person_id, person_name in rows:
            all_people[person_id] = person_name

        self._all_people = all_people

        print("There are %d people in db" % len(self._all_people))

    def get_all_encodings(self):
        all_encodings = {"unknown": []}
        query = "select fip_id, fip_encodings, fip_person_id from faces_in_pictures where fip_top is not NULL"

        rows = db.execute_select(query)

        for fip_id, encodings, person_id in rows:
            if person_id is None:
                person_id = "unknown"

            if person_id not in all_encodings:
                all_encodings[person_id] = []

            encodings = [float(enc) for enc in json.loads(encodings)]
            all_encodings[person_id].append((fip_id, encodings))

        self._all_encodings = all_encodings

        print("There are %d detected faces, %d unknown" % \
              (len(rows), len(self._all_encodings["unknown"])))

    def find_similarity_hits(self, encoding, person_id):
        np_enc = np.asarray(encoding)
        sims = []
        for _, known_enc in self._all_encodings[person_id]:
            sim = np.linalg.norm(np_enc - known_enc)**2
            if sim < config.face_recognition["hit_distance"]:
                sims.append(sim)

        return sims

    def recognize(self, encoding):
        c_person_id, c_hits = (None, 0)
        for person_id in self._all_encodings.keys():
            if person_id == "unknown":
                continue

            sims = self.find_similarity_hits(encoding, person_id)
            hits = len(sims)

            if c_hits < hits:
                c_hits = hits
                c_person_id = person_id

        if c_hits < config.face_recognition["min_number_of_hits"]:
            return None, c_hits

        return c_person_id, c_hits

    def recognize_unknowns(self):
        guesses = []
        for fip_id, encoding in self._all_encodings["unknown"]:
            person_id, _ = self.recognize(encoding)
            if person_id is not None:
                guesses.append((fip_id, person_id))

        return guesses


def recognize_faces():
    fr = FaceRecognizer()
    guesses = fr.recognize_unknowns()
    print("Recognized %d faces, trying to write to db" % len(guesses))

    c = db.connect()
    query = "update faces_in_pictures set fip_person_id_guess=:person_id where fip_id=:fip_id"
    for fip_id, person_id in guesses:
        c.execute(query, {"fip_id": fip_id, "person_id": person_id})

    c.commit()
