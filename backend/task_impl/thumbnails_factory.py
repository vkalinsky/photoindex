import os
import subprocess
from PIL import Image
from db import db, storage, utility
import config


def _createThumbnailForVideo(path, thumbnailPath, orientation):
    ffmpeg_cmdline = [
        'ffmpeg',
        '-v', 'quiet',
        '-hide_banner',
        # '-autorotate', '0',
        '-i', path,
        '-vf', 'scale=-1:%d' % config.thumbnail_size,
        '-an',
        '-frames', '1',
        '-y', thumbnailPath
    ]

    subprocess.check_call(ffmpeg_cmdline)


def _rotate_pil_image(im, orientation):
    """
    From https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
        1 = Horizontal (normal)
        2 = Mirror horizontal
        3 = Rotate 180
        4 = Mirror vertical
        5 = Mirror horizontal and rotate 270 CW (90 CCW)
        6 = Rotate 90 CW (270 CCW)
        7 = Mirror horizontal and rotate 90 CW (270 CCW)
        8 = Rotate 270 CW (90 CCW)
    """

    transpose = {
        1: [],
        2: [Image.FLIP_LEFT_RIGHT],
        3: [Image.ROTATE_180],
        4: [Image.FLIP_TOP_BOTTOM],
        5: [Image.FLIP_LEFT_RIGHT, Image.ROTATE_90],
        6: [Image.ROTATE_270],
        7: [Image.FLIP_TOP_BOTTOM, Image.ROTATE_90],
        8: [Image.ROTATE_90],
    }

    tr = transpose[orientation]
    for t in tr:
        im = im.transpose(t)

    return im


def _createThumbnailForImage(path, thumbnailPath, orientation):
    im = Image.open(path)
    if im is None:
        raise Exception("Can't open file")

    thumbnailSize = config.thumbnail_size
    im.thumbnail((thumbnailSize, thumbnailSize))

    if orientation is not None:
        im = _rotate_pil_image(im, orientation)

    with open(thumbnailPath, "w") as f:
        rgb_im = im.convert('RGB')
        rgb_im.save(f)


def make_thumbnail(item_id):
    """
    Makes thumbnail for the item

    item_id: str
        Item of the item

    Result: boolean
        True if thumbnail is created, False if not

    """

    thumbnailPath = storage.thumbnailFilename(item_id)
    if os.path.isfile(thumbnailPath):
        if os.path.getsize(thumbnailPath) > 0:
            print("Thumbnail for %s already exists" % item_id)
            return False
        else:
            print("Thumbnail for %s exists but is empty" % item_id)
    
    utility.makedirs(thumbnailPath)
    fullPath = storage.getOriginalFilename(item_id)
    fs_media_type, fs_orientation = db.fetchItemInfo(item_id, ["fs_media_type", "fs_orientation"])

    if fs_media_type == "image":
        _createThumbnailForImage(fullPath, thumbnailPath, fs_orientation)
    elif fs_media_type == "video":
        _createThumbnailForVideo(fullPath, thumbnailPath, fs_orientation)
    else:
        raise Exception("Media type %s is not supported" % fs_media_type)

    return True
