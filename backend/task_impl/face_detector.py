import json
from scipy import ndimage
import face_recognition

import config
from db import db, utility


def normalize_landmarks(landmarks, width, height):
    new_landmarks = {}
    for lm_id, lm_points in landmarks.items():
        new_lm_points = []
        for x, y in lm_points:
            x = x / width
            y = y / height
            new_lm_points.append([x, y])

        new_landmarks[lm_id] = new_lm_points

    return new_landmarks


def find_faces(item_id):
    faces = db.execute_select("select fip_id from faces_in_pictures where fip_fs_id=:id", id=item_id)
    if len(faces) != 0:
        print("Faces already detected for", item_id)
        return

    item_info = db.fetchItemInfo(item_id, [
        "fs_dir",
        "fs_filename",
        "fs_media_type",
        "fs_frame_width",
        "fs_frame_height",
        "fs_orientation",
    ])

    directory, filename, media_type, src_width, src_height, orientation = item_info

    if media_type != "image":
        print("Media type %s is not supported" % media_type)
        return

    full_path = '/'.join([config.datadir, directory, filename])

    print("Face detection in '%s' started" % full_path)

    img = face_recognition.load_image_file(full_path)

    scale_factor = 1.0

    max_size = config.face_detection.get("process_scaled_to", None)
    if max_size is not None:
        largest_side = max(img.shape[0], img.shape[1])
        scale_factor = min(float(max_size) / largest_side, 1)

        if scale_factor < 1:
            zoom_factors = [scale_factor, scale_factor, 1]  # "1" to prevent scaling along [r,g,b] axis
            img = ndimage.zoom(img, zoom_factors, order=0)

            print("Rescaled %dx%d to %dx%d (%0.2f)" % (src_width, src_height, src_width * scale_factor, src_height * scale_factor, scale_factor))

    locations = face_recognition.face_locations(img)

    if len(locations) == 0:
        db.insert_values("faces_in_pictures", {"fip_fs_id": item_id})

    w, h = utility.transformFrameSize(src_width, src_height, orientation)
    w = float(w)
    h = float(h)

    scaled_width = w * scale_factor
    scaled_height = h * scale_factor

    encodings = face_recognition.face_encodings(img, locations)
    landmarks = face_recognition.face_landmarks(img, locations)

    for i in range(len(locations)):
        top, right, bottom, left = locations[i]
        norm_landmarks = normalize_landmarks(landmarks[i], scaled_width, scaled_height)

        db_values = {
            "fip_fs_id": item_id,
            "fip_right": right / scaled_width,
            "fip_left": left / scaled_width,
            "fip_top": top / scaled_height,
            "fip_bottom": bottom / scaled_height,
            "fip_encodings": json.dumps([str(en) for en in encodings[i]]),
            "fip_landmarks": json.dumps(norm_landmarks),
        }

        db.insert_values("faces_in_pictures", db_values)

    print("Face detection in %s done, found %d faces" % (item_id, len(locations)))

    return len(locations)
