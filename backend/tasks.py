from functools import wraps
import time

from celery import Celery
from kombu import Queue, Exchange
import redlock
from db import db
import config
from redis_util import TaskContext, Task

db_ver = db.get_db_version()
if config.database_version != db_ver:
    raise Exception("DB version mismatched, current %d, required %d" % (db_ver, config.database_version))

app = Celery("tasks", broker='amqp://guest@rabbitmq//')
app.conf.task_queues = [
    Queue('celery', Exchange('celery'), routing_key='celery', queue_arguments={'x-max-priority': 10})
]

dlm = redlock.Redlock([{
    "host": config.redis_host,
    "port": config.redis_port,
    "db": config.redis_db
}])


class DLMLock:
    def __init__(self, lock_key):
        self._lock_key = lock_key
        self._lock = False

    def __enter__(self):
        self._lock = dlm.lock(self._lock_key, config.task_lock_ttl * 1000)
        if self.acquired:
            print("DLM lock %s acquired" % self._lock_key)
        else:
            print("DLM lock %s not acquired" % self._lock_key)

        return self

    def __exit__(self, tp, val, tb):
        if self._lock is not False:
            dlm.unlock(self._lock)

        print("DLM lock %s released" % self._lock_key)

    @property
    def key(self):
        return self._lock_key

    @property
    def acquired(self):
        return self._lock is not False


def run_dlm_locked(fn):
    @wraps(fn)
    def _decorator(*args, **kwargs):
        lock_key = fn.__name__ + "(" + ','.join([str(a) for a in args]) + ")"
        with DLMLock(lock_key) as lock:
            if not lock.acquired:
                return None

            res = fn(*args, **kwargs)
            return res

    return _decorator


@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def import_file(path):
    from task_impl import importer
    metadata = importer.import_file(path)

    if metadata is None:
        # File already imported, no need to run tasks
        return

    t = Task(metadata["id"], "generate_thumbnail", time.time())
    t.write_to_redis()
    generate_thumbnail.apply_async((metadata["id"],), priority=config.task_priorities["generate_thumbnail"])

    media_type = metadata["media_type"]

    opt_priority = config.task_priorities.get("optimize_" + media_type, 0)
    t = Task(metadata["id"], "optimize", time.time())
    t.write_to_redis()
    optimize.apply_async((metadata["id"],), priority=opt_priority)

    if media_type == "video":
        opt_priority = config.task_priorities.get("generate_filmstrip", 0)
        t = Task(metadata["id"], "generate_filmstrip", time.time())
        t.write_to_redis()
        generate_filmstrip.apply_async((metadata["id"],), priority=opt_priority)

    if media_type == "image":
        t = Task(metadata["id"], "find_faces", time.time())
        t.write_to_redis()
        find_faces.apply_async((metadata["id"],), priority=config.task_priorities["find_faces"])

@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def optimize(item_id):
    with TaskContext(item_id, "optimize", ttl=3600*4):
        from task_impl import optimizer
        return optimizer.optimize(item_id)


@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def optimize_and_overwrite(item_id):
    with TaskContext(item_id, "optimize", ttl=3600*4):
        from task_impl import optimizer
        return optimizer.optimize(item_id, True)


@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def generate_thumbnail(item_id):
    with TaskContext(item_id, "generate_thumbnail"):
        from task_impl import thumbnails_factory
        return thumbnails_factory.make_thumbnail(item_id)


@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def find_faces(item_id):
    with TaskContext(item_id, "find_faces"):
        from task_impl import face_detector
        return face_detector.find_faces(item_id)


@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def generate_filmstrip(item_id):
    with TaskContext(item_id, "generate_filmstrip", ttl=3600*4):
        from task_impl import video_filmstrip
        return video_filmstrip.create_film_strip(item_id)


# Maintenance

@app.task(acks_late=True, reject_on_worker_lost=True)
@run_dlm_locked
def fix_timestamp(item_id):
    with TaskContext(item_id, "fix_timestamp", ttl=3600):
        from task_impl import importer
        importer.fix_timestamp(item_id)
    