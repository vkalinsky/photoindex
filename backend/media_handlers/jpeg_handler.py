import datetime
import re
import traceback

import exifread
from PIL import Image

from . import exif_gps
from .base import DBKeys, TypeHandler


class JPEGHandler(TypeHandler):
    def __init__(self, filename):
        super(JPEGHandler, self).__init__(filename)

    def fixDateTime(self, d):
        dtrx = "[0-9]{4}:[0-9]{2}:[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}"
        if not re.match(dtrx, d):
            d = d.replace('.', ':')  # Some like to delimit date with dots
            comps = re.split("[ :]", d)
            comps = [c for c in comps if c.strip()]
            if len(comps) == 5:  # Add seconds if needed
                comps.append("00")

            comps[1] = comps[1][:2]
            comps = [('0' + c) if len(c) < 2 else c for c in comps]
            newd = ':'.join(comps[:3]) + ' ' + ':'.join(comps[3:])
            if not re.match(dtrx, newd):
                newd = None

            return newd, newd != d
        else:
            return d, False

    def get_metadata(self):
        with open(self._filename, 'rb') as f:
            try:
                exif_tags = exifread.process_file(f)
                exif_tags["JPEGThumbnail"] = ""
            except Exception:
                print(traceback.format_exc())
                return None

            metadata = {}

            lat, lng, alt = exif_gps.get_exif_location(exif_tags)
            if lat is not None and lng is not None:
                metadata[DBKeys.DB_KEY_GPS_LAT] = lat
                metadata[DBKeys.DB_KEY_GPS_LON] = lng

            if alt is not None:
                metadata[DBKeys.DB_KEY_GPS_ALT] = alt

            if "EXIF DateTimeOriginal" in exif_tags:
                dt = str(exif_tags["EXIF DateTimeOriginal"])
                dtf, wasfixed = self.fixDateTime(dt)
                if wasfixed:
                    if dtf is None:
                        print("Could not fix datetime for %s: %s" % (self._filename, dt))
                        dtf = "1984:01:01 00:00:00"
                    else:
                        print("DateTime was fixed for %s: %s -> %s" % (self._filename, dt, dtf))

                if "EXIF OffsetTimeDigitized" in exif_tags:
                    timezone = str(exif_tags["EXIF OffsetTimeDigitized"])
                    dtf = dtf + " " + timezone

                    parsed_dtf = datetime.datetime.strptime(dtf, "%Y:%m:%d %H:%M:%S %z")
                else:
                    parsed_dtf = datetime.datetime.strptime(dtf, "%Y:%m:%d %H:%M:%S")

                metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL] = parsed_dtf.isoformat()
            if "EXIF ExifImageWidth" in exif_tags \
                and "EXIF ExifImageLength" in exif_tags \
                and exif_tags["EXIF ExifImageWidth"].values[0] > 0 \
                and exif_tags["EXIF ExifImageLength"].values[0] > 0:
                metadata[DBKeys.DB_KEY_FRAME_WIDTH] = int(str(exif_tags["EXIF ExifImageWidth"]))
                metadata[DBKeys.DB_KEY_FRAME_HEIGHT] = int(str(exif_tags["EXIF ExifImageLength"]))
            else:
                f.seek(0)
                im = Image.open(f)
                metadata[DBKeys.DB_KEY_FRAME_WIDTH] = im.size[0]
                metadata[DBKeys.DB_KEY_FRAME_HEIGHT] = im.size[1]

            if "Image Orientation" in exif_tags:
                metadata[DBKeys.DB_KEY_IMAGE_ORIENTATION] = exif_tags["Image Orientation"].values[0]

            metadata[DBKeys.DB_KEY_MEDIA_TYPE] = DBKeys.METADATA_MEDIA_TYPE_IMAGE

            return metadata
