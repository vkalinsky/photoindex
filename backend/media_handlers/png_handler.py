import datetime
import re
from PIL import Image

from .base import DBKeys, TypeHandler

class PNGHandler(TypeHandler):
    def __init__(self, filename):
        super(PNGHandler, self).__init__(filename)

    def get_date_taken(self):
        date_pattern = re.compile(rb"<photoshop:DateCreated>(.*?)</photoshop:DateCreated>")
        
        try:
            # Open the file in binary mode
            with open(self._filename, "rb") as file:
                content = file.read()
            
            # Search for the date within the tag
            match = date_pattern.search(content)
            if match:
                date_str = match.group(1).decode("utf-8")  # Decode binary to string
                
                # Convert to a datetime object
                try:
                    date_obj = datetime.datetime.fromisoformat(date_str)
                    return date_obj
                except ValueError:
                    raise Exception(f"Invalid date format: {date_str}")
            else:
                raise Exception("Date not found in file")
        except FileNotFoundError:
            raise Exception(f"File not found: {self._filename}")
        except Exception as e:
            raise Exception(f"An error occurred: {e}")


    def get_metadata(self):
        metadata = {}

        with open(self._filename, 'rb') as f:
            f.seek(0)
            im = Image.open(f)
            metadata[DBKeys.DB_KEY_FRAME_WIDTH] = im.size[0]
            metadata[DBKeys.DB_KEY_FRAME_HEIGHT] = im.size[1]

        metadata[DBKeys.DB_KEY_MEDIA_TYPE] = DBKeys.METADATA_MEDIA_TYPE_IMAGE

        metadata[DBKeys.DB_KEY_DATE_TAKEN_LOCAL] = self.get_date_taken()

        return metadata