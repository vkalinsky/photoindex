import re
from decimal import Decimal

def _convert_to_degress(value):
    """
    Helper function to convert the GPS coordinates stored in the EXIF to degress in float format
    :param value:
    :type value: exifread.utils.Ratio
    :rtype: float
    """
    d = float(value.values[0].num) / float(value.values[0].den)
    m = float(value.values[1].num) / float(value.values[1].den)
    s = float(value.values[2].num) / float(value.values[2].den)

    return d + (m / 60.0) + (s / 3600.0)


def get_exif_location(exif_data):
    """
    Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)
    """
    lat = None
    lon = None

    gps_latitude = exif_data.get('GPS GPSLatitude', None)
    gps_latitude_ref = exif_data.get('GPS GPSLatitudeRef', None)
    gps_longitude = exif_data.get('GPS GPSLongitude', None)
    gps_longitude_ref = exif_data.get('GPS GPSLongitudeRef', None)
    gps_altitude = exif_data.get("GPS GPSAltitude", None)

    if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
        lat = _convert_to_degress(gps_latitude)
        if gps_latitude_ref.values[0] != 'N':
            lat = 0 - lat

        lon = _convert_to_degress(gps_longitude)
        if gps_longitude_ref.values[0] != 'E':
            lon = 0 - lon

    if gps_altitude is not None:
        alt = gps_altitude.values[0].num/gps_altitude.values[0].den
    else:
        alt = None

    return lat, lon, alt


class DMSDegree(object):
    """ An object representing a DMS Degree """

    def __init__(self, degrees, minutes=None, seconds=None, fraction=None, sign=None):
        if fraction is not None:
            if seconds is not None:
                seconds += fraction
            elif minutes is not None:
                minutes += fraction
            else:
                degrees += fraction
        minutes = Decimal(minutes) if (minutes is not None) else 0
        seconds = Decimal(seconds) if (seconds is not None) else 0
        degrees = Decimal(degrees)
        decimal = degrees + minutes / Decimal('60') + seconds / Decimal('3600')
        decimal = decimal * Decimal(sign + '1')

        self.degrees, self.minutes, self.seconds, self.sign = degrees, minutes, seconds, sign
        self.decimal = decimal


def parse_iso6709_location(value):
    """ Convert a string of coordinates to a set of DMSDegree and Decimal Altitude """
    re_coord = r"""
                ^
                (?P<lat_sign>\+|-)
                (?P<lat_degrees>[0,1]?\d{2})
                (?P<lat_minutes>\d{2}?)?
                (?P<lat_seconds>\d{2}?)?
                (?P<lat_fraction>\.\d+)?
                (?P<lng_sign>\+|-)
                (?P<lng_degrees>[0,1]?\d{2})
                (?P<lng_minutes>\d{2}?)?
                (?P<lng_seconds>\d{2}?)?
                (?P<lng_fraction>\.\d+)?
                (?P<alt>\+\d+)?
    """
    regex = re.compile(re_coord, flags=re.VERBOSE)
    match = regex.match(value).groupdict()
    results = {}
    for key in ('lat', 'lng'):
        results[key] = {}
        for value in ('sign', 'degrees', 'minutes', 'seconds', 'fraction'):
            results[key][value] = match['{}_{}'.format(key, value)]

    alt = Decimal(match['alt']) if match['alt'] is not None else Decimal(0)
    return (
        float(DMSDegree(**results['lat']).decimal),
        float(DMSDegree(**results['lng']).decimal),
        float(alt))
