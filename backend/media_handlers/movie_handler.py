import json
import subprocess

from dateutil import parser

from .base import DBKeys, TypeHandler
from .exif_gps import parse_iso6709_location


class MovieHandler(TypeHandler):
    def get_metadata(self):
        ffmpeg_cmdline = [
            'ffprobe',
            '-hide_banner', '-v', 'quiet',
            '-show_streams',
            '-show_format',
            '-print_format', 'json',
            '-select_streams', 'v:0',
            self._filename]

        p = subprocess.Popen(ffmpeg_cmdline, stdout=subprocess.PIPE, shell=False)
        res = p.wait()
        if res != 0:
            return None

        file_metadata_str = p.stdout.read()
        file_metadata = json.loads(file_metadata_str)

        md = {}

        if len(file_metadata["streams"]) == 0:
            return None

        vs = file_metadata["streams"][0]
        vs_tags = vs.get("tags", {})
        fmt = file_metadata["format"]
        fmt_tags = fmt.get("tags", {})

        iso6709_loc = fmt_tags.get("com.apple.quicktime.location.ISO6709", None)
        if iso6709_loc is not None:
            lat, lng, alt = parse_iso6709_location(iso6709_loc)
            md[DBKeys.DB_KEY_GPS_LAT] = lat
            md[DBKeys.DB_KEY_GPS_LON] = lng
            md[DBKeys.DB_KEY_GPS_ALT] = alt

        md[DBKeys.DB_KEY_MEDIA_TYPE] = "video"
        md[DBKeys.DB_KEY_FRAME_WIDTH] = vs["coded_width"]
        md[DBKeys.DB_KEY_FRAME_HEIGHT] = vs["coded_height"]
        md[DBKeys.DB_KEY_DURATION] = fmt["duration"]

        ct = None
        if "com.apple.quicktime.creationdate" in fmt_tags:
            ct = fmt_tags["com.apple.quicktime.creationdate"]
        elif "creation_time" in vs_tags:
            ct = vs_tags["creation_time"]
        elif "creation_time" in fmt_tags:
            ct = fmt_tags["creation_time"]

        try:
            md[DBKeys.DB_KEY_DATE_TAKEN_LOCAL] = parser.parse(ct).isoformat()
        except Exception:
            print("Could not parse datetime string '%s'" % ct)

        for sd in vs.get("side_data_list", []):
            rotation = sd.get("rotation", None)

            # Rotations are from https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
            # These are not handled:
            # 2 = Mirror horizontal
            # 4 = Mirror vertical
            # 5 = Mirror horizontal and rotate 270 CW (90 CCW)
            if rotation is not None:
                rot2orient = {
                    0: 1,
                    -90: 8,  # Rotate 270 CW (90 CCW)
                    180: 3,  # Rotate 180
                    -180: 3,  # Rotate 180
                    90: 6,  # Rotate 90 CW (270 CCW)
                }

                md[DBKeys.DB_KEY_IMAGE_ORIENTATION] = rot2orient.get(rotation, 1)

        return md
