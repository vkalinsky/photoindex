class DBKeys:
    DB_KEY_DATE_TAKEN = "date_taken" # UTC timestamp in ISO format
    DB_KEY_DATE_TAKEN_LOCAL = "date_taken_local" # Local timestamp in ISO format
    DB_KEY_FILENAME = "filename"
    DB_KEY_DIR = "dir"
    DB_KEY_GPS_LON = "gps_lon"
    DB_KEY_GPS_LAT = "gps_lat"
    DB_KEY_GPS_ALT = "gps_alt"
    DB_KEY_FILE_SIZE = "file_size"
    DB_KEY_MEDIA_TYPE = "media_type"
    DB_KEY_FRAME_WIDTH = "frame_width"
    DB_KEY_FRAME_HEIGHT = "frame_height"
    DB_KEY_IMAGE_ORIENTATION = "orientation"
    DB_KEY_DURATION = "duration"
    METADATA_MEDIA_TYPE_IMAGE = "image"
    METADATA_MEDIA_TYPE_VIDEO = "video"


class TypeHandler(object):

    def __init__(self, filename):
        self._filename = filename

    def get_metadata(self):
        raise "NOTIMPL"

    def get_creation_date(self):
        raise "NOTIMPL"
