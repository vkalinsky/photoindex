#!/bin/bash

service nginx start 
service cron start

nice -n 9 celery --app tasks worker --prefetch-multiplier 1 --concurrency=4 &

./index_library.py &

uwsgi --ini /home/photoindex/uwsgi.ini &

tail -f /var/log/nginx/access.log
