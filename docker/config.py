dbpath = "/photoindex/db/gallery.sqlite"
datadir = "/photoindex/data"
workdir = "/photoindex/workdir"

database_version = 5

host = "0.0.0.0"
face_detection = {
    # Images will be rescaled to this size before detecting faces
    "process_scaled_to": 1600
}

face_recognition = {
    "hit_distance": 0.25,
    "min_number_of_hits": 3
}

# Thumbnails will fit a square of this size (in pixels)
thumbnail_size = 200

# Animated preview for videos, width in pixels
filmstrip_size = 180

redis_host = "redis"
redis_port = 6379
redis_db = 0

task_lock_ttl = 10  # Seconds

task_priorities = {
    "generate_thumbnail": 8,
    "generate_filmstrip": 7,
    "optimize_image": 6,
    "optimize_video": 5,
    "find_faces": 3
}

face_preview_max_size = 128
face_preview_expand_ratio = {
    "top": 1,
    "bottom": 0.1,
    "left": 0.3,
    "right": 0.3
}
