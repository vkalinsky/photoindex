FROM node:16 as frontend-builder

WORKDIR frontend
COPY frontend .
RUN npm install -y
RUN npm run build

# Main image

FROM python:3.10-slim-buster

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y build-essential ffmpeg vim nginx curl cron cmake

WORKDIR /home/photoindex

COPY backend/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# nginx/uwsgi integration
COPY docker/uwsgi.ini /home/photoindex/
COPY docker/uwsgi_entrypoint.py /home/photoindex/
COPY docker/nginx/imgserver-uwsgi.conf /etc/nginx/sites-available/default

# built frontend files
COPY --from=frontend-builder /frontend/build/ /var/www/html/

# imgserver itself
COPY backend /home/photoindex/
COPY docker/config.py /home/photoindex/
COPY docker/entrypoint.sh /home/photoindex/
ENV PYTHONPATH=/home/photoindex/

RUN adduser --uid 1000 photoindex
RUN chown -R photoindex:photoindex ./

EXPOSE 80

CMD ["./entrypoint.sh"]
