## Build
    docker build -t photoindex .

## Run
External directories:

 - directory with gallery.sqlite, mounted to `/home/photoindex/db`
 
 - directory with images, mounted to `/home/photoindex/data`

 - work directory where thumbnails and optimized images are stored, mounted to `/home/photoindex/workdir`

Internal webserver listens standard http port `80`.

    docker run -it --rm \
      --name photoindex \
      -v $(pwd):/home/photoindex/db \
      -v ~/Pictures:/home/photoindex/data \
      -v ~/Documents/photoindex_workdir:/home/photoindex/workdir \
      -p 5500:80 photoindex
      
## Index/re-index
    docker exec -it photoindex /home/photoindex/index_library.py
    
## Run optimizer
    docker exec -it photoindex /home/photoindex/optimizer.py

