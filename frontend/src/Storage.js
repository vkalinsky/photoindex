import EXIFImageOrientation from "./EXIFImageOrientation";

export default class Storage
{
    constructor()
    {
        this.id = Math.round(Math.random()*1000);
        this.items = null;
        this.selectedItems = {};
        this.currentPath = null;
        this.filter = null;
        this.highlightedAlbums = {};

        this.setUpdateCallback(null);
    }

    setUpdateCallback(callback)
    {
        this.updateCallback = callback;
    }

    updateState(newState)
    {
        if(newState.selectedItems) {
            this.highlightedAlbums = this.getHighlightedAlbums(Object.values(newState.selectedItems));
        }
        if(this.updateCallback)
            this.updateCallback(newState);
    }

    getHighlightedAlbums(forItems) {
        let albums = {};
        for(var i=0; i<forItems.length; i++) {
            let item = forItems[i];
            console.log("item: ", item);
            if( item.albums ) {
                for( var j in item.albums ) {
                    albums[item.albums[j]] = true;
                }
            }
        }
        console.log("highlighted albums: ", albums)
        return albums;
    }

    setItems(dirInfo, items)
    {
        this.dirInfo = dirInfo;
        this.items = items;
        this.items.forEach( (v,idx) => {
            v["index"] = idx
            if(!v["frame_size_transformed"]) {
                const fs = EXIFImageOrientation.transformFrameSize(v["orientation"], [v["frame_width"], v["frame_height"]])

                v["frame_width"] = fs[0]
                v["frame_height"] = fs[1]
                v["frame_size_transformed"] = true
            }
        });
        this.selectedItems = {};
        this.updateState({items: this.items});
    }

    pathIsAlbum(path) {
        return path.startsWith(".album:");
    }

    async loadDir(path, filter)
    {
        if( this.currentPath !== path || this.filter !== filter )
        {
            const encoded_path = path.replace(/\//g, ":");
            let url = `/api/dirs/${encoded_path}/items`
            if(filter != "") {
                url += "?filter=" + encodeURIComponent(filter);
            }
            const data = await this.apiRequest(url);
            const items = data["items"]
            const dirInfo = data["dir_info"]
            this.setItems(dirInfo, items);
            this.clearSelection();
            this.currentPath = path;
            this.filter = filter;
        }
        else
        {
            return Promise.resolve();
        }
    }

    getItemById(id)
    {
        for( var i=0; i<this.items.length; i++ )
            if( this.items[i].id === id )
                return this.items[i];

        return null;
    }

    apiRequest(url, method="GET", jsonbody=null)
    {
        var params = {method: method};
        if(jsonbody)
        {
            params["body"] = JSON.stringify(jsonbody);
            params["headers"] = {
                'Content-Type': 'application/json'
            };
        }

        return fetch(url, params)
                .then((resp) => {
                    if( !resp.ok )
                        throw new Error("exception: request failed")

                    return resp.json();
                });
    }

    // Item operations
    clearSelection()
    {
        this.selectedItems = {};
        this.updateState({ selectedItems: this.selectedItems });
    }

    selectIndex(item_idx)
    {
        var item = this.items[item_idx];

        var newSelectedItems = Object.assign({}, this.selectedItems);
        if( item !== null )
        {
            newSelectedItems = {};
            newSelectedItems[item.id] = item;
        }
        else
        {
            newSelectedItems = {};
        }

        this.selectedItems = newSelectedItems;
        this.updateState({ selectedItems: this.selectedItems });
    }

    selectIndices(start, end) {
        if(start > end) {
            var tmp = start;
            start = end;
            end = tmp - 1;
        } else {
            start += 1
        }

        var newSelectedItems = Object.assign({}, this.selectedItems);
        for(var i=start; i<=end; i++) {
            var item = this.items[i];
            if( ! item.id in newSelectedItems )
                delete newSelectedItems[item.id];
            else
                newSelectedItems[item.id] = item;
        }

        this.selectedItems = newSelectedItems;
        this.updateState({ selectedItems: this.selectedItems });
    }

    toggleSelectIndex(index) {
        var item = this.items[index];
        var newSelectedItems = Object.assign({}, this.selectedItems);
        if( item.id in newSelectedItems )
            delete newSelectedItems[item.id];
        else
            newSelectedItems[item.id] = item;

        this.selectedItems = newSelectedItems;
        this.updateState({ selectedItems: this.selectedItems });
    }

    numberOfSelected()
    {
        return Object.keys(this.selectedItems).length;
    }

    applyToSelected(fn)
    {
        for(var id in this.selectedItems)
            fn(this.selectedItems[id]);
    }

    firstSelectedIndex()
    {
        var index = this.items.length + 1;
        this.applyToSelected( (im) => {
            if(index > im.index)
                index = im.index;
        });

        return index;
    }

    lastSelectedIndex()
    {
        var index = 0;
        this.applyToSelected( (im) => {
            if(index < im.index)
                index = im.index;
        });

        return index;
    }

    moveSelection(direction, append)
    {
        var nextIndex = 0;
        if( this.numberOfSelected() > 0 )
        {
            if(direction > 0)
                nextIndex = this.lastSelectedIndex() + 1;
            else if(direction < 0)
                nextIndex = this.firstSelectedIndex() - 1;

            // No wrapping
            if(nextIndex >= this.items.length || nextIndex < 0)
                return;
        }

        this.selectIndex(nextIndex);
    }

    selectNextItem(append) {
        this.moveSelection(+1, append);
    }

    selectPrevItem(append) {
        this.moveSelection(-1, append);
    }

    getNextItem(item, direction)
    {
        const item_idx = item.index + direction;
        if(item_idx >= 0 && item_idx < this.items.length)
            return this.items[item_idx];
        else
            return null;
    }

    deleteItem(item)
    {
        if(this.pathIsAlbum(this.currentPath)) {
            return this.apiRequest(`/api/albums/${this.currentPath}/items/${item.id}`, "DELETE")
        } else {
            return this.apiRequest(`/api/images/${item.id}`, "DELETE")
            .then( () => {
                var newItems = this.items.filter((im) => im.id !== item.id);
                this.setItems(this.dirInfo, newItems);
            });
        }
    }

    deleteSelected()
    {
        let deleted = new Set();
        let promises = Object.entries(this.selectedItems).map( ([item_id, item]) => {
            return this.deleteItem(item)
                .then( 
                    (r) => {
                        deleted.add(item_id);
                    },
                    () => {})
                .catch((e) => {
                    console.log("deleteSelected error", e);
                });
        });

        Promise
        .all(promises)
        .finally( () => {
            this.selectedItems = {};
            this.updateState({ selectedItems: this.selectedItems });
        });
    }

    removeSelectedFromAlbum() {
        const selected = Object.entries(this.selectedItems);
        const removedIds = new Set();
        const promises = selected.map( ([item_id, item]) => {
            return this.apiRequest(`/api/albums/${this.dirInfo.album_id}/items/${item.id}`, "DELETE")
            .then(
                (r) => {
                    removedIds.add(item_id)
                },
                () => {})
            .catch((e) => {
                console.log("removeSelectedFromAlbum error", e);
            });
        });

        Promise
        .all(promises)
        .finally( () => {
            const newItems = this.items.filter((im) => !removedIds.has(im.id));
            this.setItems(this.dirInfo, newItems);
            this.clearSelection();
        });
    }

    starItem(item, starValue)
    {
        const action = starValue ? "star" : "unstar";
        const url = `/api/images/${item.id}`;
        return this.apiRequest(url, "POST", {"action": action}).then((r) => {
            item.starred = r.starred;
            this.updateState({items: this.items});
        });
    }

    starSelected(starValue)
    {
        let stars = [];
        let selected = Object.entries(this.selectedItems);
        let promises = selected.map( ([item_id, item]) => {
            return this.starItem(item, starValue)
                .then(
                    (r) => {
                        stars.push([item.index, r.starred]);
                    },
                    () => {})
                .catch((e) => {
                    console.log("starSelected error", e);
                });
        });

        return Promise
            .all(promises)
            .finally( () => {
                this.updateState({items: this.items});
            });
    }

    selectAll()
    {
        var newSelectedItems = {};
        for(var i in this.items)
        {
            var item = this.items[i];
            newSelectedItems[item.id] = item;
        }

        this.selectedItems = newSelectedItems;

        this.updateState({
            selectedItems: this.selectedItems
        });
    }

    setAlbums(albums) {
        this.albums = albums;
        this.updateState({ albums: this.albums });
    }

    fetchAlbums() {
        this.apiRequest("/api/albums")
        .then( (albums) => {
            this.setAlbums(albums);
        })
        .catch( (e) => {
            console.log("fetchAlbums error", e);
        });
    }

    fetchAlbum(albumId) {
        return this.apiRequest(`/api/albums/${albumId}`)
    }

    saveAlbum(album) {
        return this.apiRequest(`/api/albums/${album.id}`, "PUT", album)
        .then(() => {
            // Patch the album in the list
            for(var i in this.albums)
            {
                var a = this.albums[i];
                if(a.id === album.id)
                {
                    this.albums[i] = album;
                    break;
                }
            }

            this.setAlbums(this.albums);
        })
    }

    getAlbumById(id) {
        for(var i in this.albums)
        {
            var album = this.albums[i];
            if(album.id === id)
                return album;
        }
    }

    async createAlbum(name) {
        const album = await this.apiRequest("/api/albums", "POST", { name: name });
        this.albums.push(album);
        this.setAlbums(this.albums);
        return album;
    }

    async deleteAlbum(albumId) {
        await this.apiRequest(`/api/albums/${albumId}`, "DELETE");
        var newAlbums = this.albums.filter((im) => im.id !== albumId);
        this.setAlbums(newAlbums);
    }

    async addItemsToAlbum(albumId, items) {
        const item_ids = items.map((item) => item.id);
        await this.apiRequest(`/api/albums/${albumId}/items`, "PUT", { "item_ids": item_ids });
        console.log(
            `Added ${items.length} items to album ${albumId}`
        )
    }

    // Testing

    getSelectedIds()
    {
        return Object.keys(this.selectedItems);
    }
}
