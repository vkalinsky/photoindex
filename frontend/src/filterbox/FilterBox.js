import React from 'react';
import { useState } from 'react';

import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

import * as Icons from 'react-feather';

import "./FilterBox.css"

export default function FilterBox(props) {
    const [expanded, setExpanded] = useState(props.filter != "");

    function toggleVisibility() {
        setExpanded(!expanded)
    }

    return (
        <div className="filterbox">
            <div className="filterbox--expand-button" onClick={toggleVisibility}>
                <Icons.ChevronUp size="14px" style={{animation: "rotation 2000ms", transform: expanded ? "rotate(180deg)" : "rotate(0deg)"}} />
            </div>
            {expanded &&
                <div className="filterbox--content">
                    <Select 
                        size="small"
                        sx={{width: "100%"}}
                        value={props.filter}
                        onChange={(e) => props.filterChanged(e.target.value)}>
                        <MenuItem value={""}>All items</MenuItem>
                        <MenuItem value={"geotagged"}>Geotagged</MenuItem>
                        <MenuItem value={"not geotagged"}>Not yet geotagged</MenuItem>
                        <MenuItem value={"only photos"}>Photos only</MenuItem>
                        <MenuItem value={"only videos"}>Videos only</MenuItem>
                        <MenuItem value={"with faces"}>With faces</MenuItem>
                        <MenuItem value={"without faces"}>Without faces</MenuItem>
                        <MenuItem value={"received media"}>Received media</MenuItem>
                    </Select>
                </div>}
        </div>
    )
}