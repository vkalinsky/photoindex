import _ from 'lodash';
import React, {Component, useState} from 'react';
import {Link} from 'react-router-dom';
import * as Icons from 'react-feather';

import IconButton from '@mui/material/IconButton';
import { TextField } from '@mui/material';

import './BrowserEntries.css';
import classnames from 'classnames';

class DirEntryIcon extends Component {
    render() {
        const size = 15;

        if (!this.props.path) {
            return <span>???</span>
        }

        if (this.props.path.startsWith('.album:')) {
            return <Icons.Image size={size} />;
        }
        switch (this.props.path) {
            case '.starred':
                return <Icons.Star size={size} />

            case '.trash':
                return <Icons.Trash2 size={size} />

            case '.recentlyadded':
                return <Icons.Zap size={size} />

            case '.map':
                return <Icons.Map size={size} />

            case '.random':
                return <Icons.Shuffle size={size} />

            default:
                return <Icons.Folder size={size} />
        }
    }
}

class DirEntry extends Component {
    render() {
        var subdirElements = null;
        if (typeof (this.props.subdirs) !== "undefined") {
            subdirElements = this.props.subdirs.map((d) => {
                return (
                    <DirEntry
                        name={d.name}
                        subdirs={d.dirs}
                        path={this.props.path + "/" + d.name}
                        selectedPath={this.props.selectedPath}
                        key={d.name}
                        depth={this.props.depth + 1}
                    />
                )
            })
        }

        var safePath;
        if (!_.isEmpty(this.props.path)) {
            safePath = this.props.path.replace(/\//g, ':');
        }

        var itemClass = classnames({
            "dir-entry": true,
            "dir-entry-selected": safePath === this.props.selectedPath,
        });

        return (
            <div>
                <div className={itemClass}>
                    {[...Array(this.props.depth)].map((x, i) => <span key={this.props.name + "-" + i} className="dir-entry--name--indent"></span>)}

                    {!(this.props.name == "" && this.props.depth == 0) && (
                        <span>
                            <DirEntryIcon path={this.props.path} />
                            <Link to={_.isEmpty(this.props.url) ? `/browse/${safePath}` : this.props.url} className="dir-entry--name">
                                {this.props.name}
                            </Link>
                        </span>)}
                </div>
                {subdirElements !== null && <div>{subdirElements}</div>}
            </div>
        )
    }
}

function NameEditTextField(props) {
    function onKeyPress(e) {
        if (e.key === 'Enter') {
            props.onEnter(e.target.value);
        } else if (e.key === 'Escape') {
            this.CancelEdit();
        }
    }

    function CancelEdit() {
        props.onCancel();
    }

    return (
        <TextField
            label="Name"
            value={props.value}
            onKeyPress={onKeyPress}
            onBlur={CancelEdit}
            variant="outlined"
            size="small"
            autoFocus
            fullWidth
        />
    );
}

function AlbumEntry({ albumId, url, path, name, selectedPath, highlighted, onContextMenu, onItemDropOnAlbum}) {
    var safePath;
    if (!_.isEmpty(path)) {
        safePath = path.replace(/\//g, ':');
    }

    const [dragDropTarget, setDragDropTarget] = useState(false);

    var itemClass = classnames({
        "dir-entry": true,
        "dir-entry-selected": safePath === selectedPath,
        "dir-entry-dragdrop-target": dragDropTarget,
        "dir-entry-highlighted": highlighted,
    });

    return (
        <div 
            className={itemClass}
            onDrop={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setDragDropTarget(false);
                onItemDropOnAlbum(e, albumId);
            }}
            onDragOver={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setDragDropTarget(true);
            }}
            
            onDragLeave={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setDragDropTarget(false);
            }}
            >
            <DirEntryIcon path={path} />
            <Link to={_.isEmpty(url) ? `/browse/${safePath}` : url} className="dir-entry--name">
                {name}
            </Link>

            {/* Button that shows context menu */}
            <IconButton
                variant="text"
                size="small"
                className="dir-entry--button"
                onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    onContextMenu(e, albumId);
                }}
            >
                <Icons.MoreVertical size={15} />
            </IconButton>
        </div>
    )
}

export {
    DirEntry,
    AlbumEntry,
    NameEditTextField,
}