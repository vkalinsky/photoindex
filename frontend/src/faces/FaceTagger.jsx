import React, { Component, PureComponent } from 'react';
import Storage from '../Storage.js';
import "./FaceTagger.css";
import classnames from 'classnames';

class FaceThumbnail extends Component
{
    constructor() {
        super();
        this.state = {
            ready: false,
            showContext: false,
        }
    }

    componentDidMount()
    {
        this.loadImage()
    }

    imageUrl() {
        if( this.state.showContext )
            return `/api/faces/${this.props.fipid}/context`
        else
            return `/api/faces/${this.props.fipid}/thumbnail`
    }

    loadImage() {
        this.setState({ready: false})
        fetch(this.imageUrl())
        .then((resp) => {
            if(resp.ok)
                setTimeout( () => this.setState({ready: true}), 100)
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if( prevProps.fipid != this.props.fipid || prevState.showContext != this.state.showContext )
            this.loadImage()
    }

    render() {    
        let classes = classnames({
            "tagger--facethumbnail": true,
            "tagger--facethumbnail--loading": !this.state.ready,
            "tagger--facethumbnail--ignored": this.props.ignored
        })

        if( !this.state.ready )
            return <div className={classes}>loading...</div>
        else
            return (
                <div className={classes}>
                    <img 
                        src={this.imageUrl()} 
                        onClick={() => this.setState({showContext: !this.state.showContext})} />
                </div>
        )
    }
}

class TagButton extends PureComponent
{
    render() {
        let classes = classnames({
            "tagger--tagbutton": true,
            "tagger--tagbutton--current": this.props.selected,
            "tagger--tagbutton--guessed": this.props.guessed
        });

        return (
        <button 
            className={classes} 
            onClick={() => this.props.onTag(this.props.person.id)}>
            {this.props.person.name} <sub>{this.props.person.count}</sub>
            </button>)
    }
}

class IgnoreButton extends PureComponent
{
    render() {
        let classes = classnames({
            "tagger--ignorebutton": true,
            "tagger--ignorebutton--ignored": this.props.ignored,
        });

        return (
        <button 
            className={classes} 
            onClick={this.props.onClick}>
            Ignore
            </button>)
    }
}

class NewPersonForm extends Component
{
    constructor()
    {
        super();
        this.state = {
            name: ""
        }
    }

    addPerson = () => {
        Storage.apiRequest(`/api/people`, "POST", {
            name: this.state.name,
            dob: "0000-00-00"
        })
        .then( () => {
            this.setState({name: ""});
            this.props.onNewPerson()
        })
    }

    render() {
        return (
            <div className="tagger--add-person">
                <input 
                    type="text" 
                    placeholder="new name" 
                    onChange={(e) => this.setState({
                        name: e.target.value
                    })}
                    value={this.state.name} />

                <button onClick={this.addPerson}>Add</button>
            </div>
        )
    }
}

class FaceTagger extends Component
{
    constructor() {
        super();
        this.state = {
            people: [],
            current_person_id: null,
            current_person_id_guess: null,
            current_person_ignored: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if( prevProps.match.params.fipid != this.props.match.params.fipid )
            this.reload()
    }

    componentDidMount() {
        this.reload()
    }

    goNext = () => {
        Storage.apiRequest(`/api/faces/untagged`)
        .then( (data) => {
            const nextId = data[Math.floor(Math.random()*data.length)];
            this.props.history.push(`/faces/tag/${nextId}`);
        })
    }

    goPrev = () => {
        //this.props.history.push(`/faces/tag/${Number(this.props.match.params.fipid)-1}`)
    }

    tagItem = (person_id) => {
        Storage.apiRequest(`/api/faces/${this.props.match.params.fipid}`, "PUT", {
            person_id: person_id
        })
        .then( () => {
            this.reload();
            this.goNext();
        })
    }

    ignoreCurrentItem = () => {
        Storage.apiRequest(`/api/faces/${this.props.match.params.fipid}`, "PUT", {
            ignore: !this.state.current_person_ignored
        })
        .then( () => {
            this.reload();
            this.goNext();
        })
    }

    reload = () => {
        Storage.apiRequest(`/api/faces/${this.props.match.params.fipid}`)
        .then((data) => {
            this.setState({
                current_person_id: data.person_id,
                current_person_id_guess: data.person_id_guess,
                current_person_ignored: data.ignored
            })
        });

        Storage.apiRequest("/api/people")
        .then((data) => {
          this.setState({
            people: data
          })
        });
    }

    render() {
        //if( this.state.current_person_id === null )
        //   return <div className="tagger"><i>no such face</i></div>

        return (
            <div className="tagger">
                <div>
                    <a onClick={this.goNext}>Next &rarr;</a>
                </div>
                <FaceThumbnail 
                    fipid={this.props.match.params.fipid}
                    ignored={this.state.current_person_ignored} />

                <div>
                    <IgnoreButton 
                        onClick={this.ignoreCurrentItem}
                        ignored={this.state.current_person_ignored} />
                </div>

                {this.state.people.map((p,i) => 
                    <TagButton 
                        key={`tag_${p.id}`}
                        person={p}
                        selected={this.state.current_person_id == p.id}
                        guessed={this.state.current_person_id_guess == p.id}
                        onTag={this.tagItem}/>)
                }
                <NewPersonForm
                    onNewPerson={this.reload}
                    key={"newperson_" + this.state.people.count} />
            </div>
        )
    }
}

export default FaceTagger;
