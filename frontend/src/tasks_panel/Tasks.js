import React, {useState, useEffect} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Storage from "../Storage.js";


function formatTime(t) {
    return new Date(t * 1000).toLocaleString();
}

export default function BasicTable() {
    const [tasks, setTasks] = React.useState([])

    useEffect(() => {
        let storage = new Storage();
        storage.apiRequest('/api/tasks')
        .then((data) => {
            // Running tasks first
            data.sort((a, b) => {
                if( a.status === "running" && b.status !== "running" )
                    return -1;
                if( a.status !== "running" && b.status === "running" )
                    return 1;
                return 0;
            });
            setTasks(data);
        });
    }, []);

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Item</TableCell>
                        <TableCell align="left">Name</TableCell>
                        <TableCell align="left">Created at</TableCell>
                        <TableCell align="left">Status</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {tasks.map((task) => (
                        <TableRow
                            key={task.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell>
                                <img height="80px" width="auto" src={`/api/thumbnails/${task.item_id}`} />
                            </TableCell>

                            <TableCell>
                                {task.name}
                            </TableCell>

                            <TableCell>
                                {formatTime(task.created_at)}
                            </TableCell>

                            <TableCell>
                                {task.status}
                            </TableCell>
                            
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
