import React, { Component } from 'react';
import Toolbar, {ToolbarButton, ToolbarSpacer} from "./Toolbar.js"
import ItemInfoView from "./iteminfo/ItemInfoView.js";
import Star from 'react-feather/dist/icons/star';
import {humanReadableSize} from './ByteSize.jsx';
import "./Viewer.css";

const MODE_FIT = 'fit'
const MODE_SCALE = 'scale'
class ZoomableImage extends Component {
    constructor() {
        super();
        this.state = {
            mode: MODE_FIT,
            background_size: 1,
            background_position_x: null,
            background_position_y: null,
        }
    }

    onClick = (event) => {
        if(this.state.mode === MODE_FIT) {
            const scale = 1
            this.setState({
                mode: MODE_SCALE,
                background_size: 1,
                background_position_x: window.innerWidth/2 - this.props.imageWidth / 2 * scale,
                background_position_y: window.innerHeight/2 - this.props.imageHeight / 2 * scale,
            })

            if(this.props.onModeChange) {
                this.props.onModeChange(MODE_SCALE)
            }
        } else {
            if(event.clientX - this.drag_start_x == 0 && event.clientY - this.drag_start_y == 0) {
                this.setState({
                    mode: MODE_FIT,
                })
            }
        }
    }

    onWheel = (event) => {
        if(this.state.mode === MODE_FIT) {
            return;
        }

        const zoom_point_x = event.clientX
        const zoom_point_y = event.clientY
        const zoom_factor = event.deltaY < 0 ? 1.05 : 0.95

        var new_background_size = this.state.background_size * zoom_factor

        // X
        const background_left = this.state.background_position_x
        const pixels_left_to_mouse = zoom_point_x - background_left
        const shift_left = pixels_left_to_mouse * (zoom_factor - 1)

        // Y
        const background_top = this.state.background_position_y
        const pixels_top_to_mouse = zoom_point_y - background_top
        const shift_top = pixels_top_to_mouse * (zoom_factor - 1)

        this.setState({
            background_size: new_background_size,
            background_position_x: this.state.background_position_x - shift_left,
            background_position_y: this.state.background_position_y - shift_top,
        })
    }

    // Dragging
    onMouseDown = (event) => {
        this.dragging = true
        this.drag_start_x = event.clientX
        this.drag_start_y = event.clientY
        this.drag_start_background_x = this.state.background_position_x
        this.drag_start_background_y = this.state.background_position_y
    }

    onMouseUp = (event) => {
        this.dragging = false
    }

    onMouseMove = (event) => {
        if(this.dragging) {
            this.setState({
                background_position_x: this.drag_start_background_x + (event.clientX - this.drag_start_x),
                background_position_y: this.drag_start_background_y + (event.clientY - this.drag_start_y),
            })
        }
    }

        // End dragging when mouse pointer leaves the element
    onMouseLeave = (event) => {
        this.dragging = false
    }

    // Start dragging when mouse pointer enters the element with the button pressed
    onMouseEnter = (event) => {
        if(event.buttons === 1) {
            this.dragging = true
            this.drag_start_x = event.clientX
            this.drag_start_y = event.clientY
            this.drag_start_background_x = this.state.background_position_x
            this.drag_start_background_y = this.state.background_position_y
        }
    }

    render() {
        let style = {
            backgroundImage: "url(" + this.props.imageURL + ")",
        }

        if(this.state.mode === MODE_FIT) {
            style.backgroundSize = 'contain'
        } else if(this.state.mode === MODE_SCALE) {
            style.backgroundSize = [
                Math.round(this.props.imageWidth * this.state.background_size) + 'px',
                Math.round(this.props.imageHeight * this.state.background_size) + 'px'
            ]

            if(this.state.background_position_x !== null) {
                style.backgroundPositionX = this.state.background_position_x + 'px'
            }
    
            if(this.state.background_position_y !== null) {
                style.backgroundPositionY = this.state.background_position_y + 'px'
            }
        }

        console.log(this.state.mode, style)

        return (<div 
            className="viewer-container--img"
            style={style}
            onClick={this.onClick}
            onMouseDown={this.onMouseDown}
            onMouseUp={this.onMouseUp}
            onMouseMove={this.onMouseMove}
            onMouseLeave={this.onMouseLeave}
            onMouseEnter={this.onMouseEnter}
            onWheel={this.onWheel} />)
    }
}

class Viewer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentIndex: this.props.currentIndex,
            showOverlayInfo: false,
            forceOriginal: false,
            updatedToOriginal: false,
            originalIsVisible: false, // It's true when original image is loaded and visible (hopefully)
            image_view_mode: MODE_FIT,
        }
        this.container = null;
    }

    componentDidMount() {
        this.container.focus();

        this.updateImageToOriginal()
    }

    downloadOriginal() {
        let url = this.getImageUrl(true);
        window.open(url + "&download=1");
    }

    onkeyup = (e) => {
        switch(e.code)
        {
            case "Escape":
                console.log("Escape")
                if( this.state.showOverlayInfo )
                {
                    this.setState({
                        showOverlayInfo: false
                    });
                }
                else {
                    this.props.onDismiss();
                }

                break;

            case "ArrowRight":
                if(this.props.onNextItem)
                    this.props.onNextItem();

                break;

            case "ArrowLeft":
                if(this.props.onPrevItem)
                    this.props.onPrevItem();

                break;

            case "KeyI":
                this.setState({
                    showOverlayInfo: !this.state.showOverlayInfo
                });
                break;

            case "KeyO":
                this.setState({
                    forceOriginal: !this.state.forceOriginal
                })
                break;

            case "Space":
                this.props.onDismiss();
                break;

            default:
                return;
        }

        e.stopPropagation();
        e.preventDefault();
    }

    getImageUrl(forceOriginal) {
        let im = this.props.item;
        return "/api/images/" + im.id + (forceOriginal ? "?forceOriginal=1" : "")
    }

    updateImageToOriginal() {
        this.originalImageLoadTimer = window.setTimeout(() => {
            fetch(this.getImageUrl(true)).then(() => {
                this.setState({
                    updatedToOriginal: true
                })
            }).then(() => {
                this.originalImageLoadTimer = setTimeout(() => {
                    this.setState({
                        originalIsVisible: true
                    })
                }, 1000);
            })
        }, 1000);
    }

    // After showing the optimized image, load the original one then show it once it's loaded
    // This way the user doesn't have to wait for the original image to load
    componentDidUpdate(prevProps, prevState) {
        if( this.props.item.media_type !== "image" ) {
            if(this.state.updatedToOriginal) {
                this.setState({
                    updatedToOriginal: false,
                    originalIsVisible: false,
                })
            }
        }
        const itemChanged = this.props.item.id !== prevProps.item.id;

        if( !this.state.forceOriginal && this.props.item.optimized && itemChanged ) {
            this.setState({
                updatedToOriginal: false,
                originalIsVisible: false,
                forceOriginal: false,
            })

            if(this.originalImageLoadTimer) {
                clearTimeout(this.originalImageLoadTimer);
                this.originalImageLoadTimer = null;
            }

            this.updateImageToOriginal()
        }
    }

    render() {
        let item = this.props.item;
        let imageURL = this.getImageUrl(this.state.forceOriginal);
        let originalImageURL = this.getImageUrl(true);

        var infoOverlay = null;
        if( this.state.showOverlayInfo )
        {
            infoOverlay = <ItemInfoView item={item} />
        }

        let mediaElement = null;
        switch(item.media_type)
        {
            case "image":
                const optimizedImageStyle = this.state.originalIsVisible 
                ? {
                    background: 'none'
                }
                : {
                    backgroundImage: "url(" + imageURL + ")"
                    
                }

                var originalImageElement = (<span />);
                if(this.state.forceOriginal || this.state.updatedToOriginal) {
                    originalImageElement = (
                        <ZoomableImage 
                            imageURL={originalImageURL}
                            imageWidth={item.frame_width}
                            imageHeight={item.frame_height}
                        />
                    )
                }
                mediaElement = (
                    <div 
                        className="viewer-container--img"
                        style={optimizedImageStyle} >
                            {originalImageElement}
                    </div>)
                break;

            case "video":
                mediaElement = (<video className="viewer-container--video" src={imageURL} autoPlay={true} controls />)
                break;

            default:
                console.warn(`Unknown media type ${item.media_type}`);
                break;
        }

        return (
            <div 
                ref={e => this.container = e}
                className="viewer-container"
                tabIndex="0"
                onKeyUp={this.onkeyup}>
                {item.starred &&
                    <div className="viewer-container--star-icon">
                        <Star />
                    </div>
                }
                {infoOverlay}
                <div className="viewer-container--img-container">
                    {mediaElement}
                </div>

                <Toolbar>
                    <ToolbarButton
                        type="star"
                        hint={item.starred ? "Unstar" : "Star"}
                        highlighted={item.starred}
                        clickHandler={
                        () => {
                            var item = this.props.item;
                            this.props.storage.starItem(item, !item.starred);
                        }
                    }/>

                    <ToolbarSpacer />

                    <ToolbarButton
                        type="info"
                        hint={this.state.showOverlayInfo ? "Hide info" : "Show info"}
                        highlighted={this.state.showOverlayInfo}
                        clickHandler={
                        () => this.setState({
                            showOverlayInfo: !this.state.showOverlayInfo
                        })
                    }/>

                    {item.optimized &&
                    <ToolbarButton
                        type="sun"
                        hint={this.state.forceOriginal ? "Show optimized" : "Show original"}
                        highlighted={this.state.forceOriginal}
                        enabled={!this.state.updatedToOriginal}
                        clickHandler={
                        () => this.setState({
                            forceOriginal: !this.state.forceOriginal
                        })
                    }/>}

                    <ToolbarButton
                        type="download"
                        hint={`Download original (${humanReadableSize(item.file_size)})`}
                        clickHandler={
                        () => this.downloadOriginal()
                    }/>

                    <ToolbarSpacer />

                    <ToolbarButton
                        type="trash-2"
                        hint="Move to trash"
                        clickHandler={
                        () => this.props.onDeleteItem(this.props.item)
                    }/>
                </Toolbar>

            </div>
        )
    }
}

export default Viewer