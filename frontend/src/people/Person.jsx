import React, {Component} from "react";
import {Link} from "react-router-dom";
import Storage from "../Storage";
import "./Person.css";
import ItemList, {GroupDelimiter} from "../common/ItemList";
import classnames from "classnames";
import UserCheck from 'react-feather/dist/icons/user-check';
import UserX from 'react-feather/dist/icons/user-x';

class FacePreview extends Component
{
	render() {
		var untagged = this.props.fip.person_id_guess === null && this.props.fip.person_id === null;
		var wrong = this.props.fip.person_id === null && this.props.fip.person_id_guess !==null && this.props.shouldbe != this.props.fip.person_id_guess;
		let classes = classnames({
			"people-list--facepreview": true,
			"people-list--facepreview--untagged": untagged,
			"people-list--facepreview--wrong": wrong,
		})

		var person_name = null;
		var person_icon = null;

		if( this.props.fip.ignored )
			person_icon = <UserX size="12" className="people-list--facepreview--ignored-icon" />

		if( this.props.id_to_people !== null )
		{
			if( this.props.fip.person_id !== null )
			{
				let name = this.props.fip.person_id;
				if( this.props.fip.person_id in this.props.id_to_people )
					name = this.props.id_to_people[this.props.fip.person_id].name;

				if( person_icon === null )
					person_icon = <UserCheck size="12" className="people-list--facepreview--tagged-icon" />

				person_name = name;
			}
			else if( this.props.fip.person_id_guess !== null )
			{
				let name = this.props.fip.person_id_guess;
				if( this.props.fip.person_id_guess in this.props.id_to_people )
					name = this.props.id_to_people[this.props.fip.person_id_guess].name;

				person_name = name
			}
			else
			{
				person_name = "unknown";
			}
		}

		return (
			<div className={classes}>
				<img src={`/api/faces/${this.props.fip.id}/thumbnail`} />
				<div className="people-list--facepreview--person-name">
					<div>{person_icon}<span> </span>{person_name}</div>
				</div>
			</div>)
	}
}

class PeopleDropdown extends Component
{
	constructor() {
		super();
		this.state = {person_id: null};
	}

	render() {
		if( this.props.people === null )
			return <span>Loading...</span>

		var tag_btn_title = this.state.person_id === null ? "Untag selected" : "Tag selected as";

		return (
			<div className="people-list--people-dropdown" >
			<button onClick={() => this.props.onTag(this.state.person_id)}>{tag_btn_title}</button>
			<select 
				className="people-list--people-dropdown--select"
				onChange={(e) => this.setState({person_id: e.target.value})}>
				<option value={null}></option>
				{this.props.people.map( (p) => <option value={p.id} key={p.id}>{p.name}</option>)}
			</select>
			</div>
			)
	}
}

export default class Person extends Component
{
	constructor() {
		super();
		this.state = {
			similars: [],
			fip: null,
			all_people: null,
			id_to_people: {},
			selected_ids: null
		};
	}

	reload() {
		Storage.apiRequest(`/api/faces/${this.props.match.params.personid}`)
		.then((data) => {
			this.setState({fip: data});
		});

		Storage.apiRequest("/api/people")
		.then((data) => {
			var id_to_people = {}
			data.forEach( (p) => {
				id_to_people[p.id] = p;
			})
			this.setState({
				all_people: data,
				id_to_people: id_to_people
			});
		});

		Storage.apiRequest(`/api/faces/${this.props.match.params.personid}/similar`)
		.then((data) => {
			this.setState({similars: data})
		});
	}

	componentDidMount() {
		this.reload();
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
        if( prevProps.match.params.personid != this.props.match.params.personid )
            this.reload();
    }

    sendBatchCmdForSelected = (cmd) => {
    	var ids = []
    	this.state.selected_ids.forEach((id) => ids.push(id))

    	var fullcmd = cmd;
    	fullcmd["ids"] = ids;

        Storage.apiRequest(`/api/faces/batch`, "PUT", fullcmd)
        .then( () => {
            this.reload();
        })
    }

    ignoreSelected = () => {
    	this.sendBatchCmdForSelected({"ignore": true});
    }

    tagSelected = (person_id) => {
    	this.sendBatchCmdForSelected({
    		person_id: person_id==="" ? null : person_id
    	});
    }

    selectionChanged = (new_selection) => {
    	this.setState({selected_ids: new_selection})
    }

    openItem = (fip_id) => {
    	this.props.history.push(`/people/${fip_id}`);
    }

	render() {
		if(this.state.fip === null)
			return <h3>loading...</h3>

		const unrecognized = [(<GroupDelimiter><h3>Unrecognized</h3></GroupDelimiter>)];
		const not_matched = [(<GroupDelimiter><h3>Recognized, but not matched</h3></GroupDelimiter>)];
		const matched = [(<GroupDelimiter><h3>Matched</h3></GroupDelimiter>)];
		const tagged = [(<GroupDelimiter><h3>Tagged</h3></GroupDelimiter>)];
		const current_person_id = this.state.fip.person_id || this.state.fip.person_id_guess;

		this.state.similars.forEach( (fip) => {
			let item = (<FacePreview 
							id={fip.id}
							key={`item_list_${fip.id}`}
							fip={fip}
							id_to_people={this.state.id_to_people}
						/>);

			/*
	
			unrecognized: p = null && g = null
			not_matched : p = null && g != null && g != person_id
			matched     : p = null && g != null && g = person_id
			tagged      : p != null

			*/

			if( fip.person_id === null )
			{
				if( fip.person_id_guess === null )
					unrecognized.push(item);
				else
				{
					if( current_person_id !== null )
					{
						if( fip.person_id_guess == current_person_id )
							matched.push(item);
						else
							not_matched.push(item);
					}
					else
						not_matched.push(item);
				}
			}
			else
			{
				tagged.push(item);
			}
		})

		var all = [];

		if(unrecognized.length > 1)
			all = all.concat(unrecognized);

		if(not_matched.length > 1)
			all = all.concat(not_matched);

		if(matched.length > 1)
			all = all.concat(matched);

		if(tagged.length > 1)
			all = all.concat(tagged);

		return (<div className="people-list">
			<h1>FIP {current_person_id}</h1>
			<FacePreview fip={this.state.fip} id_to_people={this.state.id_to_people} />
			<PeopleDropdown people={this.state.all_people} onTag={this.tagSelected} />
			<div>
				<button 
					className="people-list--ignore-button" 
					onClick={this.ignoreSelected}>Ignore selected</button>
			</div>

			<ItemList multiselect onDoubleClick={this.openItem} onSelectionChanged={this.selectionChanged}>
				{all}
			</ItemList>

			</div>)
	}
}