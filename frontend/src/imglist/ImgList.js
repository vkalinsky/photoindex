import React, {Component} from 'react';
import Toolbar, {ToolbarButton, ToolbarSpacer} from "../Toolbar.js";
import ItemInfoView from "../iteminfo/ItemInfoView.js";
import GeoTagger from '../geotagging/GeoTagger.js';
import { ImgListItem } from './ImgListItem';

import "./ImgList.css";

class ImgList extends Component {
  constructor() {
    super()
    this.state = {
      itemHeight: 120,
      showInfo: false,
      showGeotagger: false,
    }

    this.lastSelectedIndex = null;
  }

  toggleSelectedItemInfo = () => {
    this.setState({showInfo: !this.state.showInfo});
  }

  toggleGeotagger = () => {
    this.setState({showGeotagger: !this.state.showGeotagger});
  }

  downloadSelectedItems = () => {
  }

  toggleStarSelected = () => {
    let allStarred = true;
    for(var item_id in this.props.selectedItems)
    {
      if(!this.props.selectedItems[item_id].starred)
      {
        allStarred = false;
        break;
      }
    }

    this.props.storage.starSelected(!allStarred);
  }

  deleteSelectedItems = () => {
    this.props.storage.deleteSelected();
  }

  removeSelectedItemsFromAlbum = () => {
    this.props.storage.removeSelectedFromAlbum();
  }

  onkeydown = (e) => {
    if( e.keyCode == 32 && e.target == document.body)
    {
      e.preventDefault();
      e.stopPropagation();
    }
  }

  onkeyup = (e) => {
    if(this.state.showGeotagger || this.state.showInfo) {
      if(e.code == "Escape") {
        this.hideModals()
      }
      return;
    }

    switch(e.code)
    {
      case "Backspace":
        this.deleteSelectedItems();
        break;
  
      case "ArrowLeft":
        this.props.storage.selectPrevItem(e.shiftKey);
        break;

      case "ArrowRight":
        this.props.storage.selectNextItem(e.shiftKey);
        break;

      case "Escape":
        this.hideModals()
        break;

      case "KeyI":
        this.toggleSelectedItemInfo();
        break;

      case "KeyA":
        this.props.storage.selectAll();
        break;

      case "KeyG":
        this.toggleGeotagger();
        break;

      case "Space":
        var idx = this.props.storage.firstSelectedIndex();
        var item = this.props.storage.items[idx];
        this.showItem(item);
        break;

      // Plus button increases item height
      case "Equal":
        this.setState({itemHeight: this.state.itemHeight + 10});
        break;

      // Minus button decreases item height
      case "Minus":
        this.setState({itemHeight: this.state.itemHeight - 10});
        break;

      default:
        break;
    }

  }

  showItem = (im) => {
    this.setState({showInfo: false});
    this.props.onSelect(im);
  }

  showTasks = (tasks) => {
    
    var tasksStr = "";

    tasks.forEach( (task) => {
      tasksStr += task.name + ": " + task.status + "\n";
    })

    alert(tasksStr)
  }

  onItemClick = (event, im) => {
    if( event.shiftKey ) {
      this.props.storage.selectIndices(this.lastSelectedIndex, im.index);
    } else if( event.ctrlKey || event.metaKey ) {
      this.props.storage.toggleSelectIndex(im.index);
    } else {
      this.props.storage.selectIndex(im.index);
    }

    this.lastSelectedIndex = im.index;
  }

  hideModals = () => {
    this.setState({
      showInfo: false,
      showGeotagger: false,
    });
  }

  render() {
    if( this.props.images === null )
      return <h3>no images</h3>

    var images = this.props.images.map( (im) => {
      if( im.deleted )
        return null;

      return (<ImgListItem
        info={im}
        key={im.id + (im.starred?"starred":"")}
        onSelect={this.toggleItemSelection}
        itemHeight={this.state.itemHeight}
        selected={im.id in this.props.selectedItems}
        onDoubleClick={this.showItem}
        onClick={this.onItemClick}
        showTasks={this.showTasks}
        onDragStart={this.props.onItemDragStart}
      />)
    })

    let nSelected = this.props.storage.numberOfSelected();
    let somethingSelected = nSelected !== 0;
    let multipleItemsSelected = nSelected > 1;

    let infoItem = null;
    if( this.state.showInfo )
    {
      if(nSelected === 1)
        infoItem = this.props.storage.items[this.props.storage.firstSelectedIndex()];
      else
        this.toggleSelectedItemInfo();
    }

    const itemPluralized = nSelected==1 ? "item" : "items";
    const isAlbum = this.props.storage.dirInfo && this.props.storage.dirInfo.is_album;

    return (
      <div
        className="imglist-container"
        tabIndex="0"
        onKeyUp={this.onkeyup}
        onKeyDown={this.onkeydown}>

        <div className="grid-view">
          <div className="grid-view-content">
            {images}
          </div>
        </div>

        {(this.state.showGeotagger || infoItem) 
          && <div 
            onClick={this.hideModals} 
            className="imglist-overlay" />}

        {this.state.showGeotagger &&
          <GeoTagger 
            title={"Set location for " + nSelected + " " + itemPluralized}
            items={Object.keys(this.props.selectedItems)}
            onSave={this.toggleGeotagger} />
        }

        {infoItem &&
        <ItemInfoView item={infoItem} />}

        {somethingSelected &&
        <Toolbar>
          <ToolbarButton
            type="star"
            hint={`Toggle star for ${itemPluralized}`}
            clickHandler={this.toggleStarSelected} />

          <ToolbarSpacer />

          <ToolbarButton
            type="maximize-2"
            hint="Show item"
            clickHandler={ () => {
                var idx = this.props.storage.firstSelectedIndex();
                var item = this.props.storage.items[idx];
                this.showItem(item);
              }
            }/>

          <ToolbarButton
            type="info"
            hint={`Show item info`}
            enabled={nSelected==1}
            highlighted={infoItem !== null}
            clickHandler={this.toggleSelectedItemInfo} disabled={multipleItemsSelected} />

          <ToolbarButton
            type="geotag"
            hint={`Geotag ${itemPluralized}`}
            enabled={nSelected > 0}
            clickHandler={this.toggleGeotagger} />

          <ToolbarButton
            type="download"
            hint={`Download original ${itemPluralized}`}
            enabled={false}
            clickHandler={this.downloadSelectedItems} />

          <ToolbarSpacer />

          {isAlbum 
          ? <ToolbarButton
            type="delete"
            hint={`Remove ${itemPluralized} from album`}
            clickHandler={this.removeSelectedItemsFromAlbum} />
          : <ToolbarButton
            type="trash-2"
            hint={`Move ${itemPluralized} to trash`}
            clickHandler={this.deleteSelectedItems} />
          }

        </Toolbar>}
      </div>
      )
  }
}

export default ImgList