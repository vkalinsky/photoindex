import React from "react";
import "./Filmstrip.css";

export default class Filmstrip extends React.Component {
    constructor() {
        super();
        this.state = {
            position: 0,
        }
    }

    onMouseMove = (e) => {
        let rect = e.target.getBoundingClientRect();
        let x = e.clientX - rect.left;

        let percent_inside = Math.round(x / this.props.itemWidth * 100)
        this.setState({position: percent_inside})
    }
    render() {
        const filmstrip_url = '/api/filmstrips/' + this.props.item.id
        const css_x_pos = -this.state.position * this.props.itemWidth + 'px'
        const css_width = this.props.itemWidth * 100 + 'px'
        let style = {
            backgroundImage: "url('" + filmstrip_url + "')",
            backgroundPosition: css_x_pos + " 0%",
            backgroundSize: css_width + " 100%"
        }
        return (
            <div
                className="filmstrip"
                onMouseMove={this.onMouseMove}
                style={style}
            />
        );
    }
}
