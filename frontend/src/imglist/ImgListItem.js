import React, { Component, PureComponent } from 'react';
import { Activity, Star, Video } from 'react-feather';
import Filmstrip from './Filmstrip.js';
import classNames from 'classnames';
import Tooltip from '@mui/material/Tooltip';

function TypeIcon(props) {
  if (props.type === "video") {
    return (
      <Tooltip title="video" arrow>
        <div className="grid-view--type-icon">
          <Video size={16} />
        </div>
      </Tooltip>
    );
  }
  else {
    return null;
  }
}
class StarIcon extends Component {
  render() {
    return (
      <div className="grid-view--star-icon">
        <Star size={24} />
      </div>
    );
  }
}

function TasksIcon({tasks, showTasks}) {
  if(!tasks) {
    // Show nothing if there're no tasks
    return null;
  }

  var hasPendingTasks = false;
  var hasRunningTasks = false;
  tasks.forEach((task) => {
    if (task.status === "pending")
      hasPendingTasks = true;
    else if (task.status === "running")
      hasRunningTasks = true;
  });

  var className = "grid-view--tasks-done";
  if (hasRunningTasks)
    className = "grid-view--tasks-running";
  else if (hasPendingTasks)
    className = "grid-view--tasks-pending";

  return (
      <div 
        className={"grid-view--tasks-icon " + className} 
        onClick={(e) => {
          e.stopPropagation();
          showTasks(tasks);
        }}>
        <Activity size={16} />
      </div>
  );
}
export class ImgListItem extends PureComponent {
  constructor() {
    super();
    this.doubleClickTimer = null;
  }

  handleClick = (event) => {
    if (this.doubleClickTimer == null) {
      this.props.onClick(event, this.props.info);


      this.doubleClickTimer = setTimeout(function (self) {
        self.doubleClickTimer = null;
      }, 500, this);
    }

    else {
      // Double click
      var dct = this.doubleClickTimer;
      this.doubleClickTimer = null;
      clearTimeout(dct);
      this.props.onDoubleClick(this.props.info);
    }

    event.stopPropagation();
  };

  render() {
    var styles = {
      width: this.props.itemWidth + "px",
      height: this.props.itemHeight + "px",
    };

    let im = this.props.info;

    let [iw, ih] = [im.frame_width, im.frame_height];
    let ar = iw / ih;
    let h = this.props.itemHeight;
    let w = Math.round(h * ar);

    styles = {
      width: w,
      height: h,
    };

    let itemClass = classNames({
      "grid-view--item": true,
      "grid-view--item--selected": this.props.selected,
      "grid-view--item--non-optimized": !im.optimized
    });

    let thumbnailURL = "/api/thumbnails/" + im.id;

    return (
        <div
          className={itemClass}
          style={styles}
          draggable
          onDragStart={(e) => { this.props.onDragStart(e, im); }}
          onClick={this.handleClick}>
          
          {this.props.info.has_filmstrip && 
            <Filmstrip item={this.props.info} itemWidth={w} />}

          <img src={thumbnailURL} style={styles} alt={im.filename} />

          <TypeIcon type={im.media_type} />

          {this.props.info.starred &&
            <StarIcon />}

          {this.props.info.tasks && this.props.info.tasks.length > 0 &&
            <TasksIcon tasks={this.props.info.tasks} showTasks={this.props.showTasks} />}
        </div>
      )
  }
}
