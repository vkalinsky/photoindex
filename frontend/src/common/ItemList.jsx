import React, {Component, PureComponent} from 'react';
import classnames from 'classnames';
import './ItemList.css';

export class GroupDelimiter extends PureComponent {
  render() {
    return <div className="items-list--group-delimiter">{this.props.children}</div>
  }
}

export default class ItemList extends Component
{
  constructor()
  {
    super();
    this.state = {
      selected_ids: new Set()
    }
    this.double_click_timer = null;
    this.last_clicked_id = null;
  }

  toggleItem = (event, item_id, append) => {
    var sel = this.state.selected_ids;

    if( sel.has(item_id) )
      sel.delete(item_id);
    else
    {
      if( ! this.props.multiselect || ! event.shiftKey)
        sel.clear()

      sel.add(item_id);
    }

    this.setState({selected_ids: sel});

    if( this.props.onSelectionChanged )
      this.props.onSelectionChanged(sel);
  }

  itemClicked = (event, item_id) =>
  {
    if( ! this.props.onDoubleClick )
    {
      this.toggleItem(event, item_id);
      return;
    }

    if( this.double_click_timer == null || this.last_clicked_id != item_id )
    {
      this.toggleItem(event, item_id);

      this.double_click_timer = setTimeout(function(self){
        self.double_click_timer = null;
      }, 500, this);
      this.last_clicked_id = item_id;
    }
    else
    {
      // Double click
      var dct = this.double_click_timer;
      this.double_click_timer = null;
      clearTimeout(dct);
      this.last_clicked_id = null;
      this.props.onDoubleClick(item_id);
    }

    event.stopPropagation();
  }

  render() {
    return (
      <div className="items-list--container" onKeyDown={this.onKeyDown}>
        {this.props.children.map( (item) => {
          if(typeof item.props === "undefined" || typeof item.props.id === "undefined")
            return item;

          let classes = classnames({
              "items-list--item": true,
              "items-list--item-selected": this.state.selected_ids.has(item.props.id)
            });
          return (
            <div className={classes} key={"item_wrapper_" + item.props.id} onClick={(e) => this.itemClicked(e, item.props.id)}>
              {item}
            </div>)
        })}
      </div>
    )
  }
}
