import _ from 'lodash';
import React, { Component } from 'react';
import './App.css';
import Storage from "./Storage.js";
import {HashRouter as Router, Route, Link, Redirect} from 'react-router-dom';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Viewer from "./Viewer.js";
import ImgList from "./imglist/ImgList.js";
import Browser from "./Browser.js";
import Stats from "./Stats.jsx";
// import AllFaces from "./Experimental/AllFaces.jsx";
import FaceTagger from "./faces/FaceTagger.jsx"
import Person from "./people/Person.jsx"
import Map from "./Map.js";
import Tasks from "./tasks_panel/Tasks.js";
import FilterBox from "./filterbox/FilterBox.js";
import AlbumMenu from './Albums/AlbumMenu.jsx';
import AlbumDeleteConfirmationDialog from './Albums/AlbumDeleteConfirmationDialog';
import AlbumDeletedDialog from './Albums/AlbumDeletedDialog';
import AlbumPropertiesDialog from './Albums/AlbumPropertiesDialog';

const darkTheme = createTheme({
  palette: {
      mode: 'dark',
  },
});

class PhotoBrowser extends Component {

  constructor() {
    super()
    this.state = {
      currentlyShownImages: null,
      items: [],
      selectedItems: {},
      selectedPath: null,
      selectedPathDescription: null,
      viewImageIndex: null,
      globalFilter: localStorage.getItem("globalFilter") || "",
      anchorEl: null,
      showFilterBox: false,
      showAlbumDeleteConfirmation: false,
      dialogDeletionResult: null,
      showAlbumProperties: false,
      albumPropertiesId: null,
    }
    this.storage = new Storage();
  }

  getPathDescription = (path) => {
    if( path === null )
      return null

    if(path.startsWith(".album:")) {
      return "Album: " + path.substring(7);
    }
    switch(path) {
      case ".starred":
        return "Starred items";
      case ".trash":
        return "Deleted items";
      case ".recentlyadded":
        return "Last 100 imported items";
      case ".random":
        return "Random items";
      default:
        return path;
    }
  }

  selectDir = (path) => {
    if( this.state.selectedPath !== path )
    {
      this.setState({
        selectedPath: path,
      })

      this.storage.loadDir(path, this.state.globalFilter);
    }
  }

  componentWillReceiveProps(newProps) {
    this.selectDir(newProps.match.params.dir);
  }

  componentDidMount() {
    this.storage.setUpdateCallback( (newState) => {
      this.setState(newState);
    });
    this.selectDir(this.props.match.params.dir);
    this.storage.fetchAlbums();
  }

  componentWillUnmount() {
    this.storage.setUpdateCallback(null);
  }

  viewItem = (item) => {
    if( item === null )
      this.props.history.push(`/browse/${this.props.match.params.dir}`)
    else
      this.props.history.push(`/browse/${this.props.match.params.dir}/${item.id}`)
  }

  viewNextItem = (item, direction) => {
    this.viewItem(this.storage.getNextItem(item, direction));
  }

  deleteItem = (item) => {
    const nextItem = this.storage.getNextItem(item, 1); // Null if end of list
    this.storage.deleteItem(item).then( () => this.viewItem(nextItem) );
  }

  setGlobalFilter = (filter) => {
    localStorage.setItem("globalFilter", filter);
    this.setState({
      globalFilter: filter,
    })

    this.storage.loadDir(this.state.selectedPath, filter);
  }

  handleAlbumContextMenu = (event, album) => {
    event.preventDefault();
    this.setState({
      anchorEl: event.currentTarget,
      selectedAlbumId: album,
    });
  }

  handleCloseAlbumMenu = () => {
    this.setState({
      anchorEl: null,
      selectedAlbumId: null
    });
  }

  handleAlbumDelete = () => {
    this.setState({
      anchorEl: null,
    })

    this.showAlbumDeleteConfirmation(this.state.selectedAlbumId);
  }

  showAlbumDeleteConfirmation = (albumId) => {
    this.setState({
      showAlbumDeleteConfirmation: true,
    })
  }

  handleAlbumProperties = () => {
    this.setState({
      anchorEl: null,
      showAlbumProperties: true,
    })

    console.log(this.storage);
  }

  handleAlbumDeleteConfirmationClose = () => {
    this.setState({
      showAlbumDeleteConfirmation: false,
      selectedAlbumId: null,
    })
  }

  handleAlbumDeleteConfirm = () => {
    this.setState({
      showAlbumDeleteConfirmation: false,
    })

    this.storage.deleteAlbum(this.state.selectedAlbumId).then( () => {
      this.setState({
        dialogDeletionResult: null,
        selectedAlbumId: null,
      })
    })
    .catch( (err) => {
      this.setState({
        dialogDeletionResult: err.message,
        selectedAlbumId: null,
      })
    });
  }

  handleSelectionDragStart = (event, item) => {
    // If item_id in selection, start dragging selection
    // Otherwise, start dragging single item
    let itemsToDrag = [item]
    if( item.id in this.state.selectedItems )
    {
      itemsToDrag = Object.values(this.state.selectedItems);
    }

    event.dataTransfer.setData("application/json", JSON.stringify(itemsToDrag));
  }

  handleItemDropOnAlbum = (event, albumId) => {
    event.preventDefault();
    const items = JSON.parse(event.dataTransfer.getData("application/json"));
    this.storage.addItemsToAlbum(albumId, items);
  }

  render() {
    var viewer = null;

    if( ! _.isUndefined(this.props.match.params.itemid) )
    {
      const item_id = this.props.match.params.itemid;
      const item_idx = this.state.items.findIndex( (item) => item.id === item_id );

      if( item_idx !== -1 )
      {
        const item = this.state.items[item_idx]
        viewer = (
          <Viewer
            item={item}
            storage={this.storage}
            onNextItem={ () => this.viewNextItem(item, 1) }
            onPrevItem={ () => this.viewNextItem(item, -1) }
            onDeleteItem={ () => this.deleteItem(item) }
            onDismiss={ () => {this.viewItem(null)}}
          />);
      }
    }

    return (
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
          <div id="container">
            <div id="left-pane">
              <Browser 
                selectedPath={this.props.match.params.dir}
                albums={this.storage.albums}
                globalFilter={this.state.globalFilter}
                onAlbumContextMenu={this.handleAlbumContextMenu}
                onItemDropOnAlbum={this.handleItemDropOnAlbum}
                storage={this.storage} />
              <FilterBox 
                filter={this.state.globalFilter}
                filterChanged={this.setGlobalFilter} />
            </div>

            {this.state.items &&
            <div id="right-pane">
            <ImgList 
              images={this.state.items}
              selectedItems={this.state.selectedItems}
              storage={this.storage}
              title={this.getPathDescription(this.state.selectedPath)}
              globalFilter={this.state.globalFilter}
              onSelect={ 
                (im) => this.viewItem(im)
              }
              onItemDragStart={this.handleSelectionDragStart} />
            </div>}

            {viewer}

            <AlbumMenu
              anchorEl={this.state.anchorEl}
              onClose={this.handleCloseAlbumMenu}
              onDelete={this.handleAlbumDelete}
              onProperties={this.handleAlbumProperties}
            />

            <AlbumDeleteConfirmationDialog
              open={this.state.showAlbumDeleteConfirmation}
              onClose={this.handleAlbumDeleteConfirmationClose}
              onConfirm={this.handleAlbumDeleteConfirm}
            />

            <AlbumDeletedDialog
              open={this.state.dialogDeletionResult !== null}
              onClose={ () => this.setState({dialogDeletionResult: null}) }
              error={this.state.dialogDeletionResult}
            />

            <AlbumPropertiesDialog
              open={this.state.showAlbumProperties}
              onClose={ () => this.setState({showAlbumProperties: false}) }
              albumId={this.state.selectedAlbumId}
              storage={this.storage}
            />
          </div>
      </ThemeProvider>
    )
  }
}

class App extends Component
{
  render() {
    return (
      <div>
        <Router>
          <div>
          <Route path="/stats" component={Stats} />
          <Route path="/tasks" component={Tasks} />
          <Route path="/browse/:dir/:itemid?" component={PhotoBrowser} />
          {/*<Route path="/experimental/allfaces" component={AllFaces} />*/}
          <Route path="/faces/tag/:fipid?" component={FaceTagger} />
          <Route path="/people/:personid" component={Person} />
          {/*<Route exact path="/people" component={AllPeople} />*/}
          <Route exact path="/" render={() => (
              <Redirect to="/browse/.recentlyadded"/>
          )}/>
          <Route exact path="/map" component={Map} />
          </div>
        </Router>
      </div>)
  }
}

export default App;
