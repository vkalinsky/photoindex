
export default class HumanReadableAge 
{
	static plural(number, unit)
	{
		if(number === 1)
			return `1 ${unit}`;
		else
			return `${number} ${unit}s`;
	}

	static age(birthday, date)
	{
		const months = date.getMonth() - birthday.getMonth()
	       + (12 * (date.getFullYear() - birthday.getFullYear()));

		const days = (date - birthday)/(86400*1000);
		const years = months / 12;

		if( days < 30 )
		{
			return `${ this.plural(Math.floor(days), "day") }`;
		}
		else if(years < 1)
		{
			return `${ this.plural(months, "month") }`
		}
		else if(years < 3)
		{
			let full_years = Math.floor(months/12);
			let rem_months = months - full_years*12;

			if(rem_months > 0)
				return `${this.plural(full_years, "year")} and ${this.plural(rem_months, "month")}`;
			else
				return `${this.plural(full_years, "year")}`;
		}
		else if(years < 8)
		{
			return `${Math.round(years*10)/10} years`;
		}

		return `${Math.floor(years)} years`;
	}
}
