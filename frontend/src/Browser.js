import _ from 'lodash';
import React, {Component} from 'react';

import Storage from './Storage.js';
import './Browser.css';
import {DirEntry, AlbumEntry, NameEditTextField} from './BrowserEntries.js';
import {Button} from '@mui/material';
import thumbsDown from 'react-feather/dist/icons/thumbs-down.js';

function SectionHeader(props) {
  return (
      <div className="browser--section-header">
          {props.children}
      </div>
  );
}

class Browser extends Component {
  constructor() {
    super()
    this.state = {
      data: null,
      albums: null,
      newAlbumNameVisible: false,
    }
  }

  fetchData() {
    let url = "/api/dirs"
    if(this.props.globalFilter !== "") {
      url += "?filter=" + encodeURIComponent(this.props.globalFilter);
    }

    this.props.storage.apiRequest(url)
    .then((data) => {
      this.setState({
        dirs: data
      })
    })
  }

  componentDidUpdate(prevProps) {
    if(prevProps.globalFilter !== this.props.globalFilter) {
      this.fetchData();
    }
  }

  componentDidMount = () => {
    this.fetchData();
  }

  addAlbum = (name) => {
    this.setState({
      newAlbumNameVisible: false,
    })

    this.props.storage.createAlbum(name)
  }

  cancelAddAlbum = () => {
    this.setState({
      newAlbumNameVisible: false,
    })
  }

  showNewAlbumUI = () => {
    this.setState({
      newAlbumNameVisible: true,
    })
  }

  render() {
    return (
      <div className="dir-browser">

      <DirEntry
        name="Recently added"
        path=".recentlyadded"
        selectedPath={this.props.selectedPath}
        depth={0}
      />

      <DirEntry
        name="Starred items"
        path=".starred"
        selectedPath={this.props.selectedPath}
        depth={0}
      />

      <DirEntry
        name="Map"
        url="/map"
        path=".map"
        depth={0}
        selectedPath={this.props.selectedPath}
      />

      <hr />

      { this.props.albums != null && this.props.albums.length > 0 &&
        <SectionHeader>Albums</SectionHeader>
      }

      { this.props.albums != null ?
        this.props.albums.map((album) => {
          return (
            <AlbumEntry
              albumId={album.id}
              name={album.name}
              path={album.path}
              highlighted={this.props.storage.highlightedAlbums.hasOwnProperty(album.id)}
              selectedPath={this.props.selectedPath}
              key={album.path}
              onContextMenu={this.props.onAlbumContextMenu}
              onItemDropOnAlbum={this.props.onItemDropOnAlbum}
            />
          )
        }) : null
      }

      { this.state.newAlbumNameVisible
          ? (
          <div className="dir-entry">
            <NameEditTextField onEnter={this.addAlbum} onCancel={this.cancelAddAlbum}></NameEditTextField>
          </div>)
          : <Button style={{"width": "100%"}} size="small" onClick={this.showNewAlbumUI}>New album</Button>
      }

      { this.state.dirs != null ?
        <div>
          <SectionHeader>Directories</SectionHeader>

          <DirEntry
            name={this.state.dirs[0].name}
            subdirs={this.state.dirs[0].dirs}
            selectedPath={this.props.selectedPath}
            path=""
            depth={0}
          />
        </div>
        :
        <p>loading</p>
      }

      <hr />

      <DirEntry
        name="Random"
        path=".random"
        depth={0}
      />

      <DirEntry
        name="Trash"
        path=".trash"
        depth={0}
      />

      </div>
    )
  }
}

export default Browser
