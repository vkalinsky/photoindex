import _ from 'lodash';

export default class EXIFImageOrientation
{
    static orientationName(value)
    {
        let orientations = {
            1: "Horizontal (normal)",
            2: "Mirror horizontal",
            3: "Rotate 180",
            4: "Mirror vertical",
            5: "Mirror horizontal and rotate 270 CW",
            6: "Rotate 90 CW",
            7: "Mirror horizontal and rotate 90 CW",
            8: "Rotate 270 CW",
        };

        if(value in orientations)
            return orientations[value];
        else
            return "Unknown (" + value + ")"
    }

    static transformFrameSize(orientation, frameSize)
    {
        if(_.isUndefined(orientation))
            return frameSize;

        var [w, h] = frameSize;
        
        switch(orientation)
        {
            case 1: // Horizontal (normal) 
            case 2: // Mirror horizontal 
            case 3: // Rotate 180 
            case 4: // Mirror vertical 
                return [w, h];

            case 5: // Mirror horizontal and rotate 270 CW 
            case 6: // Rotate 90 CW 
            case 7: // Mirror horizontal and rotate 90 CW 
            case 8: // Rotate 270 CW
                return [h, w];

            default:
                console.log("Wrong EXIF orientation value", orientation);
                return [w, h]
        }
    }
}