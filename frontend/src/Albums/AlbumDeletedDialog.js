import React from 'react';

import {Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button} from '@mui/material';

export default function AlbumDeletedDialog({open, error, onClose}) {
    return (
        <Dialog
            open={open}
            onClose={onClose}>
            <DialogTitle>Delete album</DialogTitle>
            {error && <DialogContent>
                <DialogContentText>
                    Could not delete the album due to error: {error}
                </DialogContentText>
            </DialogContent>}
            {!error && <DialogContent>
                <DialogContentText>
                    Album deleted.
                </DialogContentText>
            </DialogContent>}
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    )
}
