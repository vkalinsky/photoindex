import React from 'react';

import {Dialog, DialogActions, DialogContent, TextField, DialogTitle, Button, DialogContentText} from '@mui/material';
import { useEffect } from 'react';

export default function({open, albumId, storage, onClose, onSave}) {
    if (!albumId) {
        return (
            <Dialog
            open={open}
            onClose={onClose}>
            <DialogTitle>NO ALBUMINFO</DialogTitle>
            </Dialog>
        )
    }

    const [albumName, setAlbumName] = React.useState('');
    const [albumStats, setAlbumStats] = React.useState(null);
    const [loadedAlbum, setLoadedAlbum] = React.useState(null);

    useEffect(() => {
        if(!open)
            return;

        storage.fetchAlbum(albumId).then((album) => {
            setAlbumName(album.name);
            setAlbumStats(album.stats);
            setLoadedAlbum(album);

        })

        return () => {
            setAlbumName('');
            setAlbumStats(null);
            setLoadedAlbum(null);
        }

    }, [albumId, open]);

    const handleNameChange = (event) => {
        setAlbumName(event.target.value);
    };

    const handleSave = () => {
        const album = loadedAlbum;
        album.name = albumName;

        storage.saveAlbum(album).then(() => {
            onClose();
        });
    };

    return (
        <Dialog
            open={open}
            onClose={onClose}>
            <DialogTitle>Edit album properties</DialogTitle>
            <DialogContent sx={{padding: "0.5em"}}>
                {/* TextField gets cropped without this empty space */}
                <DialogContentText>&nbsp;</DialogContentText>
                <TextField
                    label="Album name"
                    value={albumName}
                    onChange={handleNameChange}
                    fullWidth
                    autoFocus
                    />

            </DialogContent>
            <DialogContent>
            {
                albumStats && <DialogContentText>{albumStats.nb_items} items are there in this album, dated {albumStats.dates.min} to {albumStats.dates.max}</DialogContentText>
            }
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                {loadedAlbum && <Button onClick={handleSave} color="primary" autoFocus>
                    Save
                </Button>}
            </DialogActions>
        </Dialog>
    )
}