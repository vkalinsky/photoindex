import React from 'react';

import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button} from '@mui/material';

export default function({open, onClose, onConfirm}) {
    return (
        <Dialog
            open={open}
            onClose={onClose}>
            <DialogTitle>Delete album</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Are you sure you want to delete this album?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={() => onConfirm()} color="primary" autoFocus>
                    Delete
                </Button>
            </DialogActions>
        </Dialog>
    )
}
