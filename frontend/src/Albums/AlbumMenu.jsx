import React from 'react';

import {Menu, MenuItem} from '@mui/material';

export default function AlbumMenu({anchorEl, onClose, onDelete, onProperties}) {
    return (
        <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={onClose}
            MenuListProps={{ 'aria-labelledby': 'context-menu' }}
        >
            <MenuItem onClick={onDelete}>Delete</MenuItem>
            <MenuItem onClick={onProperties}>Properties</MenuItem>
        </Menu>
    )
}