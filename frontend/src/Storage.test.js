import Storage from "./Storage.js"

function populateStorage(nb_items)
{
    var items = [];

    for( var i=0; i<nb_items; i++ )
    {
        items.push({
            date_taken: new Date(1500000000*1000 + 3600*i),
            dir: "/Finland",
            file_size: i + 1,
            filename: `DSC0_${i}.JPG`,
            id: `item-${i}`,
            media_type: "image",
            optimized: false,
            orientation: 1
        });
    }

    Storage.setItems(items);
}

test("selection", () => {
    populateStorage(5);

    Storage.selectItemWithIndex(0);
    expect(Storage.getSelectedIds()).toEqual(["item-0"]);

    Storage.selectItemWithIndex(1);
    expect(Storage.getSelectedIds()).toEqual(["item-1"]);
    
    Storage.selectItemWithIndex(2);
    Storage.selectItemWithIndex(3, true);
    expect(Storage.getSelectedIds()).toEqual(["item-2", "item-3"]);

    Storage.selectItemWithIndex(1);
    expect(Storage.getSelectedIds()).toEqual(["item-1"]);
});

test("deleting", () => {
    populateStorage(10);
    Storage.selectItemWithIndex(0, true);
    Storage.selectItemWithIndex(1, true);
    Storage.selectItemWithIndex(2, true);
    Storage.setUpdateCallback( () => console.log("updated") )
    Storage.deleteSelected();
    Storage.selectAll();
    console.log(Storage.getSelectedIds());
});
