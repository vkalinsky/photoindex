import React, {Component} from 'react';
import Star from 'react-feather/dist/icons/star';
import EXIFImageOrientation from "../EXIFImageOrientation.js";
import "./ItemInfoView.css";
import HumanReadableAge from "../humanReadableAge.js";

class ChildAge extends Component {
    render() {
        if( !this.props.birthday || !this.props.date )
            return null;

        var birthDayDate = new Date(this.props.birthday);
        var dateTakenDate = new Date(this.props.date);

        if( dateTakenDate < birthDayDate )
            return null;

        return (
            <p className="iteminfoview--child-age">
                {this.props.name} is {HumanReadableAge.age(birthDayDate, dateTakenDate)}
            </p>
        )
    }
}

class ItemInfoViewMain extends Component {
    render() {
        let im = this.props.item;
        let hasGPS = ("gps_lat" in im && "gps_lon" in im);
        let mapLink = !hasGPS ? "" : ("http://www.google.com/maps/place/" + im.gps_lat + "," + im.gps_lon);

        return (
            <div className="iteminfoview--main">
                <div className="iteminfoview--main--thumbnail">
                    <img alt={im.filename} src={"/api/thumbnails/" + im.id} />
                </div>
                <div className="iteminfoview--main--info">
                    <h1>{im.dir + "/" + im.filename}</h1>

                    {im.date_taken &&
                    <p>Taken {im.date_taken}</p>}

                    <ChildAge name="Masha" birthday="2015-10-27" date={im.date_taken} />
                    <ChildAge name="Alexey" birthday="2010-06-30" date={im.date_taken} />
                    <ChildAge name="Sasha" birthday="2004-02-12" date={im.date_taken} />

                    {hasGPS &&
                        <p>{im.geo_tagged ? "Tagged location" : "Location"}: <a href={mapLink} target="_blank">{im.gps_lon}, {im.gps_lat}</a></p>
                    }
                </div>
                {im.starred &&
                <div className="iteminfoview--main--star">
                    <Star />
                </div>}
            </div>
        )
    }
}

function fileSizeSI(a,b,c,d,e){
    return (b=Math,c=b.log,d=1e3,e=c(a)/c(d)|0,a/b.pow(d,e)).toFixed(2)
    +' '+(e?'kMGTPEZY'[--e]+'B':'Bytes')
}

class ItemInfoViewStat extends Component {
    render() {
        if(this.props.details === null)
            return null;

        let im = this.props.item;
        let details = this.props.details;
        return (
            <div className="iteminfoview--stat">
            {"optimized" in details && (
                <div className="iteminfoview--stat--row">
                    <div className="iteminfoview--stat--cell">Optimized: yes,
                    &nbsp;
                    {fileSizeSI(im.file_size)} &rarr; {fileSizeSI(details.optimized.size)},
                    &nbsp;
                    {details.optimized.ratio}x
                    </div>
                </div>
            )}
                <div className="iteminfoview--stat--row">
                    <div className="iteminfoview--stat--cell">
                    Orientation: {EXIFImageOrientation.orientationName(im.orientation)}
                    </div>
                </div>
            </div>
        )
    }
}

class ItemInfoViewRaw extends Component {
    render() {
        if(this.props.details === null)
            return null;

        return (
            <div className="iteminfoview--raw">
                <pre>{JSON.stringify(this.props.details, null, 2)}</pre>
            </div>
        )
    }
}

export default class ItemInfoView extends Component {
    constructor() {
        super();
        this.item_id = null;
        this.state = {
            info: null
        };
    }

    componentDidUpdate() {
        let im = this.props.item;

        if( this.item_id !== im.id )
        {
            this.item_id = im.id;
            this.setState({
                info: {}
            });
            fetch("/api/images/" + im.id + "/details")
                .then( (resp) => resp.json() )
                .then( (data) => {
                    this.setState({
                        info: data
                    })
                });
        }
    }

    componentDidMount() {
        this.componentDidUpdate();
    }

    render() {
        let im = this.props.item;

        return (
            <div className="iteminfoview">
                <ItemInfoViewMain item={im} />
                <ItemInfoViewStat item={im} details={this.state.info} />
                <ItemInfoViewRaw item={im} details={this.state.info} />
            </div>
        );
    }
}