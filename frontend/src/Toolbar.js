import React, {Component} from 'react';
import './Toolbar.css';
import * as Icons from 'react-feather';

export class ToolbarButton extends Component {

    getIconByType(typeName)
    {
        let icon = null;
        switch(typeName)
        {
            case "_spacer": 
                break;

            case "star": 
                icon = <Icons.Star />;
                break;

            case "sun": 
                icon = <Icons.Sun />;
                break;

            case "maximize-2": 
                icon = <Icons.Maximize2 />;
                break;

            case "info":
                icon = <Icons.Info />;
                break;

            case "geotag":
                icon = <Icons.MapPin />;
                break;

            case "download":
                icon = <Icons.Download />;
                break;

            case "trash-2":
                icon = <Icons.Trash2 />;
                break;

            case "delete":
                icon = <Icons.FileMinus />;
                break;

            default:
                break;
        }
        return icon;
    }

    handleClick = (e) => {
        if(!this.props.disabled)
        {
            this.props.clickHandler();
            e.stopPropagation();
        }
    }

    render() {
        if( this.props.enabled === false )
            return null;

        var className = "toolbar--button";
        if( this.props.disabled )
            className += " toolbar--button--disabled";
        else if( this.props.highlighted )
            className += " toolbar--button--highlighted";

        return (
        <div className={className} onClick={this.handleClick}>
            {this.props.hint &&
                <span className="toolbar--tooltip">
                    {this.props.hint}
                </span>}
            {this.getIconByType(this.props.type)}
        </div>);
    }
}

export class ToolbarSpacer extends Component {
    render() {
        return <div className="toolbar--spacer" />
    }
}

export default class Toolbar extends Component {
    render() {
        return (
        <div className="toolbar">
        {this.props.children}
        </div>);
    }
}
