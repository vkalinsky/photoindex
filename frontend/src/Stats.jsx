import React, {Component, PureComponent} from 'react';
import Storage from "./Storage.js";
import {Link} from 'react-router-dom';
import ByteSize from './ByteSize.jsx';

import "./Stats.css";

class Percents extends Component {
    constructor() {
        super();
        this.state = {detailed: false}
    }

    render() {
        // return <span>{this.props.number} of {this.props.outof}</span>

        var display_value = null;
        if(this.props.number === 0)
            display_value = "None"
        else if(this.props.number === this.props.outof)
            display_value = "All"
        else
            display_value = `${Math.round(this.props.number * 100 / this.props.outof)}%`

        return (
            <span 
                className="stats--percent-value"
                onClick={ () => { this.setState({detailed: !this.state.detailed})} }
                >
                {display_value}
                {this.state.detailed && (<span>
                    &nbsp;({this.props.number} of {this.props.outof})
                </span>)}
        </span>)
    }
}

export default class Stats extends Component {
    constructor() {
        super();
        this.state = {stats: null}
    }

    componentDidMount() {
        Storage.apiRequest("/api/stats")
        .then( (data) => {
            this.setState({stats: data})
        });
    }

    render() {
        let stats = this.state.stats;

        if( ! stats )
            return <p>loading...</p>

        const total_items = stats.media_types.image + stats.media_types.video;

        return (<div className="stats stats_container">
            <h1>Storage</h1>
            Total <ByteSize bytes={stats.storage.total} />,
            <ByteSize bytes={stats.total_size} /> used by photos,
            <ByteSize bytes={stats.storage.free} /> free

            <h1>Library</h1>
            <p>The database itself is <ByteSize bytes={stats.db.file_size} /></p>
            <p>{total_items} items in library: {stats.media_types.image} photos, {stats.media_types.video} videos</p>
            {/*<p>Library is <ByteSize bytes={} />: <ByteSize bytes={} /> in photos and <ByteSize bytes={} /> in video</p>*/}

            <h1>Caches</h1>
            <p>There's <ByteSize bytes={stats.workdir.optimized.size} /> of optimized items and <ByteSize bytes={stats.workdir.thumbnails.size} /> of thumbnails.</p>
            <p><Percents number={stats.workdir.optimized.count} outof={total_items} /> of items have optimized versions</p>
            <p><Percents number={stats.workdir.thumbnails.count} outof={total_items} /> of items have thumbnails</p>

            <h1>Trash</h1>
            <p><ByteSize bytes={0} /> in {stats.trashes} items.</p>
            <p><Link to="/browse/.trash">Click here to see what's there</Link></p>
        </div>);
    }
}