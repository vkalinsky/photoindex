import React, {PureComponent} from 'react';

function humanReadableSize(size)
{
    const units = ["bytes", "KB", "MB", "GB", "TB", "PB", "EB", null];
    var uii = 0;
    while(size >= 1000 && units[uii+1])
    {
        size /= 1024;
        uii++;
    }

    size = Math.floor(size)

    return `${size} ${units[uii]}`;
}

class ByteSize extends PureComponent {

    render() {
        return <span className="size">{humanReadableSize(this.props.bytes)}</span>
    }
}

export default ByteSize
export { humanReadableSize };