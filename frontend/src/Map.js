import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import 'mapbox-gl/dist/mapbox-gl.css';
import * as Icons from 'react-feather';

import "./Map.css";

mapboxgl.accessToken = 'pk.eyJ1IjoidmthbGluc2t5IiwiYSI6ImNsN2p1YjRzYTBqbDIzeW1xNnkwdjZzc3EifQ.7t_kWRag6TBOfNNCyBMU6A';

class Marker extends Component {
    render() {
        return <div className="marker">
            {this.props.children}
        </div>
    }
}

export default class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lng: -75.8,
            lat: 45.3,
            zoom: 5
        };
        this.mapContainer = React.createRef();
    }

    componentDidMount() {
        const { lng, lat, zoom } = this.state;
        const map = new mapboxgl.Map({
            container: this.mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: zoom
        });

        map.on('move', () => {
            this.setState({
                lng: map.getCenter().lng.toFixed(4),
                lat: map.getCenter().lat.toFixed(4),
                zoom: map.getZoom().toFixed(2)
            });
        });

        map.on('load', () => {
            // Add a new source from our GeoJSON data and
            // set the 'cluster' option to true. GL-JS will
            // add the point_count property to your source data.
            map.addSource('pictures', {
                type: 'geojson',
                data: "/api/locations",
                cluster: true,
                clusterMaxZoom: 14, // Max zoom to cluster points on
                clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
            });

            map.addLayer({
                id: 'clusters',
                type: 'circle',
                source: 'pictures',
                filter: ['has', 'point_count'],
                paint: {
                    // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
                    // with three steps to implement three types of circles:
                    //   * Blue, 20px circles when point count is less than 100
                    //   * Yellow, 30px circles when point count is between 100 and 750
                    //   * Pink, 40px circles when point count is greater than or equal to 750
                    'circle-color': [
                        'step',
                        ['get', 'point_count'],
                        '#51bbd6',
                        100,
                        '#f1f075',
                        750,
                        '#f28cb1'
                    ],
                    'circle-radius': [
                        'step',
                        ['get', 'point_count'],
                        20,
                        100,
                        30,
                        750,
                        40
                    ]
                }
            });

            map.addLayer({
                id: 'cluster-count',
                type: 'symbol',
                source: 'pictures',
                filter: ['has', 'point_count'],
                layout: {
                    'text-field': '{point_count_abbreviated}',
                    'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                    'text-size': 12
                }
            });

            map.addLayer({
                id: 'unclustered-point',
                type: 'circle',
                source: 'pictures',
                filter: ['!', ['has', 'point_count']],
                paint: {
                    'circle-color': '#11b4da',
                    'circle-radius': 10,
                    'circle-stroke-width': 3,
                    'circle-stroke-color': '#fff'
                }
            })

            map.on('mouseenter', 'clusters', () => {
                map.getCanvas().style.cursor = 'pointer';
            });

            map.on('mouseenter', 'unclustered-point', () => {
                map.getCanvas().style.cursor = 'pointer';
            });
            

            map.on('mouseleave', 'clusters', () => {
                map.getCanvas().style.cursor = '';
            });

            map.on('mouseleave', 'unclustered-point', () => {
                map.getCanvas().style.cursor = '';
            });

            map.on('click', 'unclustered-point', (e) => {
                const coordinates = e.features[0].geometry.coordinates.slice();
                const fs_id = e.features[0].properties.id;
                const fs_dir = e.features[0].properties.dir;
                const fs_dir_safe = fs_dir.replace(/\//g, ':')

                // Ensure that if the map is zoomed out such that
                // multiple copies of the feature are visible, the
                // popup appears over the copy being pointed to.
                while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                }

                var preview_url = `/api/thumbnails/${fs_id}`
                var view_link = `/#/browse/${fs_dir_safe}/${fs_id}`
                new mapboxgl.Popup()
                    .setLngLat(coordinates)
                    .setHTML(
                        `<a href=${view_link}><img src=${preview_url} width="160" height="auto" /></a>`
                    )
                    .addTo(map);
            });
        })

        
    }

    render() {
        return (
            <div className="map-container" >
                <div ref={this.mapContainer} className="map-container" />
                <div className="map-back-to-pictures">
                    <Link to="/" ><Icons.ArrowLeftCircle size={34}/></Link>
                </div>
            </div>
        );
    }
}
