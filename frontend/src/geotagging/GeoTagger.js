import React, { Component } from "react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { StylesControl, ZoomControl } from 'mapbox-gl-controls';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

import "./GeoTagger.css";
import GeoStorage from "./Api.js";

const mapBoxApiToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';
mapboxgl.accessToken = mapBoxApiToken;

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '50%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    zIndex: 100,
};

const nowhere = {"id": "none", "name": "Jump to...", "lon": 0, "lat": 0};

export default class GeoTagger extends Component {
    constructor(props) {
        super(props);

        // Ottawa ON coordinates
        let initial_lat = 45.4215;
        let initial_lon = -75.6972;
        let initial_zoom = 6;

        let savedLocation = localStorage.getItem("lastGeoLocation");
        if (savedLocation !== undefined) {
            savedLocation = JSON.parse(savedLocation);
            if(savedLocation !== null
                && savedLocation.lat !== undefined
                && savedLocation.lon !== undefined
                && savedLocation.zoom !== undefined) {
                initial_lat = savedLocation.lat;
                initial_lon = savedLocation.lon;
                initial_zoom = savedLocation.zoom;
            }
        }

        this.state = {
            locations: [],
            currentLocation: nowhere,
            lat: initial_lat,
            lon: initial_lon,
            zoom: initial_zoom,
            locationName: '',
            ready: false,
            in_progress: false,
            mapStyle: 0,
        }
        this.mapContainer = React.createRef();
        this.geoStorage = new GeoStorage();
        this.geoStorage.loadLocations().then((locations) => {
            var locationsDict = {};
            locations.forEach((location) => {
                locationsDict[location.id] = location;
            });
            locationsDict[nowhere.id] = nowhere;

            this.setState({
                locations: locationsDict,
                ready: true
            });
        });

        console.log(this.props.items);
    }

    saveLocation = (location) => {
        localStorage.setItem("lastGeoLocation", JSON.stringify(location));
    }

    componentDidMount() {
        const map = new mapboxgl.Map({
            container: this.mapContainer.current,
            center: [this.state.lon, this.state.lat],
            style: 'mapbox://styles/mapbox/streets-v11',
            zoom: this.state.zoom
        });

        const geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl
        });
        map.addControl(geocoder, 'top-left');
        map.addControl(new ZoomControl(), 'top-right');
        map.addControl(new StylesControl({
            styles: [
                {
                    label: 'M',
                    styleName: 'Mapbox Streets',
                    styleUrl: 'mapbox://styles/mapbox/streets-v11',
                }, {
                    label: 'S',
                    styleName: 'Satellite',
                    styleUrl: 'mapbox://styles/mapbox/satellite-streets-v11',
                },
            ],
        }), 'bottom-left');

        map.on("move", () => {
            let location = {
                lon: map.getCenter().lng.toFixed(4),
                lat: map.getCenter().lat.toFixed(4),
                zoom: map.getZoom().toFixed(2),
            }
            this.setState(location)
            this.saveLocation(location)

            if (this.state.currentLocation !== nowhere) {
                if (this.state.currentLocation.lon !== this.state.lon || this.state.currentLocation.lat !== this.state.lat) {
                    this.setState({currentLocation: nowhere});
                }
            }
        });

        this.map = map;
    }

    saveCurrentLocation = () => {
        return this.geoStorage.addLocation(this.state.locationName, this.state.lat, this.state.lon, this.state.zoom)
        .then((loc) => {
            var newLocations = this.state.locations
            newLocations[loc.id] = loc;
            this.setState({
                currentLocation: loc,
                locations: newLocations
            });
        })
    }

    setLocation = () => {
        this.setState({in_progress: true});
        this.geoStorage.setItemsLocation(this.props.items, this.state.lat, this.state.lon, this.state.zoom).then(() => {
            if(this.state.locationName !== '') {
                return this.saveCurrentLocation()
            }
        })
        .then(() => {
            if(this.props.onSave)
                this.props.onSave();
        })
        .catch((err) => {
            alert("Request failed: " + err);
        })
        .finally(() => {
            this.setState({in_progress: false});
        });
    }

    goToKnowLocation = (event) => {
        const newLocation = this.state.locations[event.target.value];
        this.map.flyTo({
            center: [newLocation.lon, newLocation.lat],
            zoom: newLocation.zoom,
        });

        this.setState({
            currentLocation: newLocation,
        });
    }

    setNewLocationName = (event) => {
        this.setState({
            locationName: event.target.value
        })
    }

    render() {
        const locationMenuItems = Object.values(this.state.locations).map((location) => {
            return <MenuItem key={`loc_${location.id}`} value={location.id}>{location.name}</MenuItem>
        });

    return (
            <div>
                <div>{this.state.ready ? "" : "Loading..."}</div>
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {this.props.title}
                    </Typography>

                    <div className="select-known-location">
                        <Select size="small" sx={{ background:'black' }} onChange={this.goToKnowLocation} value={this.state.currentLocation.id}>
                        {locationMenuItems}
                        </Select>
                    </div>

                    <div ref={this.mapContainer} className="geotagger-map-container">
                        <div className="center-marker" />

                    </div>

                    <div className="ui-row-delimiter" />

                    <div className="save-button-container">
                        <TextField
                            size="small"
                            sx={{verticalAlign: 'baseline'}}
                            type="search"
                            label="Save as new location"
                            onChange={this.setNewLocationName}
                            value={this.state.locationName}
                            />

                        <div className="ui-col-delimiter" />

                        <Button
                            variant="contained"
                            disabled={!this.state.ready || this.state.in_progress}
                            onClick={this.setLocation}>
                            Set location
                        </Button>
                    </div>
                </Box>
            </div>
        )
    }
}