import Storage from '../Storage.js';

export default class GeoStorage {
    constructor() {
        this.storage = new Storage();
    }

    loadLocations() {
        return this.storage.apiRequest("/api/locations/named").then(data => {
            return data.locations;
        });
    }

    getKnownLocations() {
        return this.storage.apiRequest('/api/locations/named');
    }

    addLocation(name, lat, lon, zoom) {
        const loc = {
            name: name,
            lat: lat,
            lon: lon,
            zoom: zoom
        }
        return this.storage.apiRequest('/api/locations/named', 'POST', loc);
    }

    deleteLocation(id) {
        return this.storage.apiRequest(`/api/locations/named/${id}`, 'DELETE');
    }

    setItemsLocation(item_ids, lat, lon, zoom) {
        return this.storage.apiRequest(`/api/images/_batch/location`, 'PATCH', {
            items: item_ids,
            lat: lat,
            lon: lon,
            zoom: zoom
        });
    }
}