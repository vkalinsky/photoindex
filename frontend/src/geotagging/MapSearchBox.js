import React, { Component } from "react";
import Autocomplete from "@mui/material/Autocomplete"
import TextField from "@mui/material/TextField"

export default class MapSearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
            input: "",
            options: [],
        };
    }

    // Load mapbox places on input change
    componentDidUpdate(prevProps, prevState) {
        if( prevState.input !== this.state.input )
        {
            fetch("https://api.mapbox.com/geocoding/v5/mapbox.places/" + this.state.input + ".json?access_token=" + this.props.mapboxToken)
                .then( (resp) => resp.json() )
                .then( (data) => {
                    this.setState({
                        options: data.features
                    })
                });
        }
    }


    render() {
        return (
            <Autocomplete
                value={this.state.value}
                onChange={(event, newValue) => {
                    this.setState({value: newValue});
                    this.props.onChange(newValue);
                }}
                inputValue={this.state.input}
                onInputChange={(event, newInputValue) => {
                    this.setState({input: newInputValue});
                }}
                id="search-box"
                options={this.state.options}
                getOptionLabel={(option) => option.place_name}
                fullWidth
                size="small"
                renderInput={(params) => <TextField {...params} label="Search" variant="outlined" />}
            />
        )
    }
}